package com.castori_de_munte.motivity.motivity_client.middleware;

import com.castori_de_munte.motivity.motivity_client.Global;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class HttpInterceptor {
    public static OkHttpClient authInterceptor(){
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.connectTimeout(Global.connectionTimeout, TimeUnit.MILLISECONDS);
        httpClient.readTimeout(Global.connectionTimeout, TimeUnit.MILLISECONDS);
        httpClient.addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request original = chain.request();

                Request request = original.newBuilder()
                        .header("Cookie", Global.authToken)
                        .method(original.method(), original.body())
                        .build();

                return chain.proceed(request);
            }
        });

        return httpClient.build();
    }
}
