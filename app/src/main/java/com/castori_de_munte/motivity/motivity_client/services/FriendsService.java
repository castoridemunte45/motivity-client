package com.castori_de_munte.motivity.motivity_client.services;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;

import com.castori_de_munte.motivity.motivity_client.Global;
import com.castori_de_munte.motivity.motivity_client.R;
import com.castori_de_munte.motivity.motivity_client.SharedFunctions;
import com.castori_de_munte.motivity.motivity_client.middleware.ServiceCreator;
import com.castori_de_munte.motivity.motivity_client.models.ActivityObj;
import com.castori_de_munte.motivity.motivity_client.models.FriendObj;
import com.castori_de_munte.motivity.motivity_client.models.ResponseList;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by andreea on 08.02.2017.
 */

public class FriendsService {
    public HashMap<String, Boolean> actions;
    private Context context;
    private com.castori_de_munte.motivity.motivity_client.interfaces.FriendsService friendsService;
    private String eventName;

    public FriendsService(Context context, String eventName){
        this.actions = new HashMap<>();
        this.context = context;
        this.eventName = eventName;
        this.friendsService =
                ServiceCreator.retrofitService(com.castori_de_munte.motivity.motivity_client.interfaces.FriendsService.class, context);
    }

    public void list(Map<String, String> options){
        if ((actions.containsKey("list") && actions.get("list")) || SharedFunctions.checkNetworkStatus(context, friendsService, eventName)) return;
        else
            actions.put("list", true);
        String url = "user/" + Global.user.getId() + "/friend";
        Call<ResponseList<FriendObj.Friend>> call = friendsService.userFriends(url,  options);
        call.enqueue(new Callback<ResponseList<FriendObj.Friend>>() {
            @Override
            public void onResponse(Call<ResponseList<FriendObj.Friend>> call, Response<ResponseList<FriendObj.Friend>> response) {
                int statusCode = response.code();
                actions.remove("list");
                Intent intent = new Intent(eventName);

                if(statusCode == 200) {
                    List<FriendObj.Friend> friends = response.body().getItems();
                    for(FriendObj.Friend friend : friends){
                        Global.friends.add(friend);
                    }
                }else {
                    String message = Global.isDev ?
                            response.message() : context.getResources().getString(R.string.error_generic);
                    intent.putExtra("error", message);
                }

                LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
            }

            @Override
            public void onFailure(Call<ResponseList<FriendObj.Friend>> call, Throwable t) {
                SharedFunctions.failing(context, t, eventName);
            }
        });
    }

    public void get(int friendId){
        if ((actions.containsKey("get") && actions.get("get")) || SharedFunctions.checkNetworkStatus(context, friendsService, eventName)) return;
        else
            actions.put("get", true);
        String url = "user/" + Global.user.getId() + "/friend/" + friendId;
        Call<FriendObj> call = friendsService.get(url);
        call.enqueue(new Callback<FriendObj>() {
            @Override
            public void onResponse(Call<FriendObj> call, Response<FriendObj> response) {
                int statusCode = response.code();
                actions.remove("get");
                Intent intent = new Intent(eventName);

                if(statusCode == 200) {
                    // For further implementation
                }else {
                    String message = Global.isDev ?
                            response.message() : context.getResources().getString(R.string.error_generic);
                    intent.putExtra("error", message);
                }

                LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
            }

            @Override
            public void onFailure(Call<FriendObj> call, Throwable t) {
                SharedFunctions.failing(context, t, eventName);
            }
        });
    }

    public void insert(FriendObj friendObj, final boolean appendToList){
        if(appendToList)
            if ((actions.containsKey("insert") && actions.get("insert")) || SharedFunctions.checkNetworkStatus(context, friendsService, eventName)) return;
            else
                actions.put("insert", true);
        String url = "user/" + Global.user.getId() + "/friend";
        Call<FriendObj> call = friendsService.insert(url,friendObj);
        call.enqueue(new Callback<FriendObj>() {
            @Override
            public void onResponse(Call<FriendObj> call, Response<FriendObj> response) {
                int statusCode = response.code();
                actions.remove("insert");
                Intent intent = new Intent(eventName);

                if(statusCode == 200) {
                    if(appendToList) Global.friends.add(response.body().getFriend());
                }else {
                    String message = Global.isDev ?
                            response.message() : context.getResources().getString(R.string.error_generic);
                    intent.putExtra("error", message);
                }

                LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
            }

            @Override
            public void onFailure(Call<FriendObj> call, Throwable t) {
                SharedFunctions.failing(context, t, eventName);
            }
        });
    }

    public void delete(final int id){
        if ((actions.containsKey("delete") && actions.get("delete")) || SharedFunctions.checkNetworkStatus(context, friendsService, eventName)) return;
        else
            actions.put("delete", true);
        String url = "user/" + Global.user.getId() + "/friend/" + id;
        Call<String> call = friendsService.delete(url);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                int statusCode = response.code();
                actions.remove("delete");
                Intent intent = new Intent(eventName);

                if(statusCode == 200) {
                    for(int i = 0; i < Global.friends.size(); i ++){
                        if(Global.friends.get(i).getId() == id){
                            Global.friends.remove(i);
                        }
                    }
                    // For further implentation
                }else {
                    String message = Global.isDev ?
                            response.message() : context.getResources().getString(R.string.error_generic);
                    intent.putExtra("error", message);
                }

                LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                SharedFunctions.failing(context, t, eventName);
            }
        });
    }

    public void update(final FriendObj friendObj){
        if ((actions.containsKey("update") && actions.get("update")) || SharedFunctions.checkNetworkStatus(context, friendsService, eventName)) return;
        else
            actions.put("update", true);
        String url = "user/" + Global.user.getId() + "/friend/" + friendObj.getFriend().getId();
        Call<FriendObj> call = friendsService.update(url, friendObj);
        call.enqueue(new Callback<FriendObj>() {
            @Override
            public void onResponse(Call<FriendObj> call, Response<FriendObj> response) {
                int statusCode = response.code();
                actions.remove("update");
                Intent intent = new Intent(eventName);

                if(statusCode == 200) {
                    FriendObj.Friend friend = response.body().getFriend();
                    for(int i = 0; i < Global.friends.size(); i ++){
                        if(Global.friends.get(i).getId() == friendObj.getFriend().getId()){
                            Global.friends.remove(i);
                            Global.friends.add(i, FriendObj.Friend.copyFriend(friend));
                        }
                    }
                }else {
                    String message = Global.isDev ?
                            response.message() : context.getResources().getString(R.string.error_generic);
                    intent.putExtra("error", message);
                }

                LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
            }

            @Override
            public void onFailure(Call<FriendObj> call, Throwable t) {
                SharedFunctions.failing(context, t, eventName);
            }
        });
    }
}
