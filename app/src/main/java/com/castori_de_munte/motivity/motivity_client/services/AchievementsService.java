package com.castori_de_munte.motivity.motivity_client.services;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;

import com.castori_de_munte.motivity.motivity_client.Global;
import com.castori_de_munte.motivity.motivity_client.R;
import com.castori_de_munte.motivity.motivity_client.SharedFunctions;
import com.castori_de_munte.motivity.motivity_client.middleware.ServiceCreator;
import com.castori_de_munte.motivity.motivity_client.models.Stats;
import com.castori_de_munte.motivity.motivity_client.models.UserAchievementObj;

import java.util.HashMap;

import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AchievementsService{
    public HashMap<String, Boolean> actions;
    private Context context;
    private com.castori_de_munte.motivity.motivity_client.interfaces.AchievementsService achievementsService;
    private String eventName;

//    @Override
//    public void onReceive(Context context, Intent intent) {
//        Bundle bundle = intent.getExtras();
//
//        if(SharedFunctions.intentHasError(context.getApplicationContext(), bundle)){
//            return;
//        }
//        checkAchievements(context);
//    }

    public AchievementsService(Context context, String eventName){
        this.actions = new HashMap<>();
        this.context = context;
        this.eventName = eventName;
        this.achievementsService =
                ServiceCreator.retrofitService(com.castori_de_munte.motivity.motivity_client.interfaces.AchievementsService.class, context);
    }

    public void checkAchievements() {
        if(Global.user == null) return;
        getStats(context);
    }

    private void getStats(final Context context) {
        String url = "stats/" + Global.user.getId();
        Call<Stats> call = achievementsService.getStats(url);
        call.enqueue(new Callback<Stats>() {
            @Override
            public void onResponse(Call<Stats> call, Response<Stats> response) {
                int statusCode = response.code();
                Intent intent = new Intent(eventName);

                if(statusCode == 200) {
                    verifyAchievements(response.body());
//                    Global.user = response.body();
                }else {
                    String message = Global.isDev ?
                            response.message() : context.getResources().getString(R.string.error_generic);
                    intent.putExtra("error", message);
                }

                LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
            }

            @Override
            public void onFailure(Call<Stats> call, Throwable t) {
                SharedFunctions.failing(context, t, eventName);
            }
        });
    }

    private void verifyAchievements(Stats stats) {
        for (int i = 0; i < Global.user.getUserAchievements().size(); i ++){
            UserAchievementObj.UserAchievement userAchievement = Global.user.getUserAchievements().get(i);
            if(userAchievement.getIsCompleted()) continue;
            if(userAchievement.getAchievement().getTarget().toLowerCase().equals("mission")){
                if(userAchievement.getAchievement().getMilestone() <= stats.getCompletedMissions()){
                    completeAchievement(userAchievement);
                }
            } else
            if(userAchievement.getAchievement().getTarget().toLowerCase().equals("activity")){
                if(userAchievement.getAchievement().getMilestone() <= stats.getActivities()){
                    completeAchievement(userAchievement);
                }
            } else
            if(userAchievement.getAchievement().getTarget().toLowerCase().equals("friend")){
                if(userAchievement.getAchievement().getMilestone() <= stats.getFriends()){
                    completeAchievement(userAchievement);
                }
            }
        }
    }

    private void completeAchievement(UserAchievementObj.UserAchievement userAchievement) {
        userAchievement.setIsCompleted(true);
        UserAchievementObj userAchievementObj = new UserAchievementObj();
        userAchievementObj.setUserAchievement(userAchievement);
        Call<UserAchievementObj> call = achievementsService.update(userAchievementObj, "user/" + Global.user.getId() + "/achievement/" + userAchievement.getId());
        call.enqueue(new Callback<UserAchievementObj>() {
            @Override
            public void onResponse(Call<UserAchievementObj> call, Response<UserAchievementObj> response) {
                int statusCode = response.code();
                Intent intent = new Intent(eventName);

                if(statusCode == 200) {
                    SweetAlertDialog sd =  new SweetAlertDialog(context);
                    sd.setCustomImage(context.getResources().getDrawable(R.drawable.ic_star_circle));
                    sd.setTitleText(response.body().getUserAchievement().getAchievement().getTitle());
                    sd.setContentText(response.body().getUserAchievement().getAchievement().getDescription());
                    sd.setCancelable(true);
                    sd.setCanceledOnTouchOutside(true);
                    sd.show();
                }else {
                    String message = Global.isDev ?
                            response.message() : context.getResources().getString(R.string.error_generic);
                    intent.putExtra("error", message);
                }

                LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
            }

            @Override
            public void onFailure(Call<UserAchievementObj> call, Throwable t) {
                SharedFunctions.failing(context, t, eventName);
            }
        });
    }
}
