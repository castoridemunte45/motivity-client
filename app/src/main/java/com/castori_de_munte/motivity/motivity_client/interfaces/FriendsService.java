package com.castori_de_munte.motivity.motivity_client.interfaces;

import com.castori_de_munte.motivity.motivity_client.models.ActivityObj;
import com.castori_de_munte.motivity.motivity_client.models.FriendObj;
import com.castori_de_munte.motivity.motivity_client.models.ResponseList;

import java.util.Map;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.QueryMap;
import retrofit2.http.Url;

public interface FriendsService {
    @GET
    Call<ResponseList<FriendObj.Friend>> userFriends(
            @Url String url, @QueryMap Map<String, String> options
    );

    @GET//("user/{user_id}/friend/{id}")
    Call<FriendObj> get(@Url String url);

    @PUT
    Call<FriendObj> update(@Url String url, @Body FriendObj friendObj);

    @POST
    Call<FriendObj> insert(@Url String url, @Body FriendObj friendObj);

    @DELETE
    Call<String> delete(@Url String url);

}
