package com.castori_de_munte.motivity.motivity_client.services;

import android.content.Context;
import android.content.Intent;
import android.os.Parcelable;
import android.support.v4.content.LocalBroadcastManager;

import com.castori_de_munte.motivity.motivity_client.Global;
import com.castori_de_munte.motivity.motivity_client.R;
import com.castori_de_munte.motivity.motivity_client.SharedFunctions;
import com.castori_de_munte.motivity.motivity_client.middleware.SerializableArray;
import com.castori_de_munte.motivity.motivity_client.middleware.ServiceCreator;
import com.castori_de_munte.motivity.motivity_client.models.ActivityObj;
import com.castori_de_munte.motivity.motivity_client.models.MissionObj;
import com.castori_de_munte.motivity.motivity_client.models.ResponseList;
import com.castori_de_munte.motivity.motivity_client.models.UserObj;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MissionsService {
    public HashMap<String, Boolean> actions;
    private Context context;
    private com.castori_de_munte.motivity.motivity_client.interfaces.MissionsService missionsService;
    private String eventName;

    public MissionsService(Context context, String eventName){
        this.actions = new HashMap<>();
        this.context = context;
        this.eventName = eventName;
        this.missionsService =
                ServiceCreator.retrofitService(com.castori_de_munte.motivity.motivity_client.interfaces.MissionsService.class, context);
    }

    public void list(Map<String, String> options){
        if ((actions.containsKey("list") && actions.get("list")) || SharedFunctions.checkNetworkStatus(context, missionsService, eventName)) return;
        else
            actions.put("list", true);
        String url = "user/" + Global.user.getId() + "/mission";
        Call<ResponseList<MissionObj.Mission>> call = missionsService.userMissions(url,  options);
        call.enqueue(new Callback<ResponseList<MissionObj.Mission>>() {
            @Override
            public void onResponse(Call<ResponseList<MissionObj.Mission>> call, Response<ResponseList<MissionObj.Mission>> response) {
                int statusCode = response.code();
                actions.remove("list");
                Intent intent = new Intent(eventName);

                if(statusCode == 200) {
                    List<MissionObj.Mission> missions = response.body().getItems();
                    for(MissionObj.Mission mission : missions){
                        Global.missions.add(mission);
                    }
//                    List<MissionObj.Mission> missions = response.body().getItems();
//                    intent.putExtra("missions", new SerializableArray<MissionObj.Mission>(missions));
                }else {
                    String message = Global.isDev ?
                            response.message() : context.getResources().getString(R.string.error_generic);
                    intent.putExtra("error", message);
                }

                LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
            }

            @Override
            public void onFailure(Call<ResponseList<MissionObj.Mission>> call, Throwable t) {
                SharedFunctions.failing(context, t, eventName);
            }
        });
    }

    public void delete(final int id){
        if ((actions.containsKey("delete") && actions.get("delete")) || SharedFunctions.checkNetworkStatus(context, missionsService, eventName)) return;
        else
            actions.put("delete", true);
        String url = "user/" + Global.user.getId() + "/mission/" + id;
        Call<String> call = missionsService.delete(url);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                int statusCode = response.code();
                actions.remove("delete");
                Intent intent = new Intent(eventName);

                if(statusCode == 200) {
                    for(int i = 0; i < Global.missions.size(); i ++){
                        if(Global.missions.get(i).getId() == id){
                            Global.missions.remove(i);
                        }
                    }
                }else {
                    String message = Global.isDev ?
                            response.message() : context.getResources().getString(R.string.error_generic);
                    intent.putExtra("error", message);
                }

                LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                SharedFunctions.failing(context, t, eventName);
            }
        });
    }

    public void update(final MissionObj missionObj){
        if ((actions.containsKey("update") && actions.get("update")) || SharedFunctions.checkNetworkStatus(context, missionsService, eventName)) return;
        else
            actions.put("update", true);
        String url = "user/" + Global.user.getId() + "/mission/" + missionObj.getMission().getId();
        Call<MissionObj> call = missionsService.update(url, missionObj);
        call.enqueue(new Callback<MissionObj>() {
            @Override
            public void onResponse(Call<MissionObj> call, Response<MissionObj> response) {
                int statusCode = response.code();
                actions.remove("delete");
                Intent intent = new Intent(eventName);

                if(statusCode == 200) {
                    Global.mission = MissionObj.Mission.copyMission(response.body().getMission());
                    for(int i = 0; i < Global.missions.size(); i++){
                        if(Global.missions.get(i).getId() == missionObj.getMission().getId()){
                            Global.missions.remove(i);
                            Global.missions.add(i, MissionObj.Mission.copyMission(missionObj.getMission()));
                        }
                    }
                }else {
                    String message = Global.isDev ?
                            response.message() : context.getResources().getString(R.string.error_generic);
                    intent.putExtra("error", message);
                }

                LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
            }

            @Override
            public void onFailure(Call<MissionObj> call, Throwable t) {
                SharedFunctions.failing(context, t, eventName);
            }
        });
    }

    public void get(int id){
        if ((actions.containsKey("get") && actions.get("get")) || SharedFunctions.checkNetworkStatus(context, missionsService, eventName)) return;
        else
            actions.put("get", true);
        Call<MissionObj> call = missionsService.get("user/" + Global.user.getId() + "/mission/" + id);
        call.enqueue(new Callback<MissionObj>() {
            @Override
            public void onResponse(Call<MissionObj> call, Response<MissionObj> response) {
                int statusCode = response.code();
                actions.remove("get");
                Intent intent = new Intent(eventName);

                if (statusCode == 200) {
                    Global.mission = MissionObj.Mission.copyMission(response.body().getMission());
                }else {
                    String message = Global.isDev ?
                            response.message() : context.getResources().getString(R.string.error_generic);
                    intent.putExtra("error", message);
                }

                LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
            }
            public void onFailure(Call<MissionObj> call, Throwable t) {
                SharedFunctions.failing(context, t, eventName);
            }
        });
    }

    public void insert(MissionObj missionObj){
        if ((actions.containsKey("insert") && actions.get("insert")) || SharedFunctions.checkNetworkStatus(context, missionsService, eventName)) return;
        else
            actions.put("insert", true);
        String url = "user/" + Global.user.getId() + "/mission";
        Call<MissionObj> call = missionsService.insert(url,missionObj);
        call.enqueue(new Callback<MissionObj>() {
            @Override
            public void onResponse(Call<MissionObj> call, Response<MissionObj> response) {
                int statusCode = response.code();
                actions.remove("insert");
                Intent intent = new Intent(eventName);

                if(statusCode == 200) {
                    Global.missions.add(0, response.body().getMission());
                }else {
                    String message = Global.isDev ?
                            response.message() : context.getResources().getString(R.string.error_generic);
                    intent.putExtra("error", message);
                }

                LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
            }

            @Override
            public void onFailure(Call<MissionObj> call, Throwable t) {
                SharedFunctions.failing(context, t, eventName);
            }
        });
    }
}

