package com.castori_de_munte.motivity.motivity_client.models;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class FriendObj {
    @JsonCreator
    public FriendObj() {
        this.friend = new Friend();
    }

    @JsonProperty("friend")
    private Friend friend;

    @JsonProperty("friend")
    public Friend getFriend() {
        return friend;
    }

    @JsonProperty("friend")
    public void setFriend(Friend friend) {
        this.friend = friend;
    }

    public static class Friend{
        @JsonCreator
        public Friend() {
            this.nickname ="";
        }

        @JsonProperty("id")
        private int id;

        @JsonProperty("user_id")
        private int userId;

        @JsonProperty("friend_id")
        private int friendId;

        @JsonProperty("nickname")
        private String nickname;

        @JsonProperty("user")
        private UserObj.User user;

        @JsonProperty("id")
        public int getId() {
            return id;
        }

        @JsonProperty("id")
        public void setId(int id) {
            this.id = id;
        }

        @JsonProperty("user_id")
        public int getUserId() {
            return userId;
        }

        @JsonProperty("user_id")
        public void setUserId(int userId) {
            this.userId = userId;
        }

        @JsonProperty("friend_id")
        public int getFriendId() {
            return friendId;
        }

        @JsonProperty("friend_id")
        public void setFriendId(int friendId) {
            this.friendId = friendId;
        }

        @JsonProperty("nickname")
        public String getNickname() {
            return nickname;
        }

        @JsonProperty("nickname")
        public void setNickname(String nickname) {
            this.nickname = nickname;
        }

        @JsonProperty("user")
        public UserObj.User getUser() {
            return user;
        }

        @JsonProperty("user")
        public void setUser(UserObj.User user) {
            this.user = user;
        }

        public static Friend copyFriend(Friend friend){
            Friend newFriend = new Friend();
            newFriend.setId(friend.getId());
            newFriend.setFriendId(friend.getFriendId());
            newFriend.setNickname(friend.getNickname());
            newFriend.setUserId(friend.getUserId());
            return newFriend;
        }
    }
}
