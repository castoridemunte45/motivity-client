package com.castori_de_munte.motivity.motivity_client;

import android.animation.ObjectAnimator;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.content.LocalBroadcastManager;
import android.view.View;
import android.view.animation.DecelerateInterpolator;
import android.view.inputmethod.InputMethodManager;
import android.widget.ProgressBar;

import com.castori_de_munte.motivity.motivity_client.custom_controls.ISO8601;
import com.castori_de_munte.motivity.motivity_client.custom_controls.SnackbarWrapper;
import com.castori_de_munte.motivity.motivity_client.models.ActivityObj;
import com.castori_de_munte.motivity.motivity_client.models.MissionObj;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class SharedFunctions {

    public static<T> boolean checkNetworkStatus(Context context, T service, String eventName){
        if(service == null) {
            Intent intent = new Intent(eventName);
            intent.putExtra("error", context.getResources().getString(R.string.no_internet));
            LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
        }
        return  service == null;
    }

    public static void failing(Context context, Throwable t, String eventName){
        Intent intent = new Intent(eventName);
        if(Global.isDev)
            intent.putExtra("error", t.getMessage());
        else intent.putExtra("error", context.getResources().getString(R.string.error_generic));
        LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
    }

    public static boolean intentHasError(Context context, Bundle bundle){
        if(bundle != null) {
            String error = bundle.getString("error");
            if (error != null) {
                SnackbarWrapper snackbarWrapper = SnackbarWrapper.make(context.getApplicationContext(),
                        Global.isDev ? error : context.getString(R.string.error_generic), Snackbar.LENGTH_SHORT);
                snackbarWrapper.show();
                return true;
            }
        }
        return false;
    }

    public static void showKeyboard(Context context, View view){
        InputMethodManager inputMethodManager =
                (InputMethodManager)context.getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManager.toggleSoftInputFromWindow(
                view.getApplicationWindowToken(),
                InputMethodManager.SHOW_FORCED, 0);
    }

    public static void hideKeyboardFrom(Context context, View view) {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Activity.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public static int calculateRewardedExp(ActivityObj.Activity activity, MissionObj.Mission mission){
        return activity.getDifficulty() * (mission.getMilestone()/5);
    }

    public static int calculateNextLevelExp(ActivityObj.Activity activity){
        return (int) Math.pow(activity.getLevel()*activity.getDifficulty(),2);
    }

    public static void setMax(ProgressBar pbProgress, int max, float factor){
        float m = max * factor;
        pbProgress.setMax((int)m);
    }

    public static void setProgress(ProgressBar pbProgress, int progress, float factor){
        float m = progress * factor;
        pbProgress.setProgress(0);
        if(android.os.Build.VERSION.SDK_INT >= 11){
            // will update the "progress" propriety of progressbar until it reaches progress
            ObjectAnimator animation = ObjectAnimator.ofInt(pbProgress, "progress", (int)m);
            animation.setDuration(750); // 0.75 seconds
            animation.setInterpolator(new DecelerateInterpolator());
            animation.start();
        }
        else
            pbProgress.setProgress((int)m); // no animation on Gingerbread or lower
    }

    public static float roundFloat(float d, int decimalPlace) {
        BigDecimal bd = new BigDecimal(Float.toString(d));
        bd = bd.setScale(decimalPlace, BigDecimal.ROUND_HALF_UP);
        return bd.floatValue();
    }
}
