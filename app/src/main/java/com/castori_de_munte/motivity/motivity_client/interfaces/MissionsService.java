package com.castori_de_munte.motivity.motivity_client.interfaces;

import com.castori_de_munte.motivity.motivity_client.models.MissionObj;
import com.castori_de_munte.motivity.motivity_client.models.ResponseList;
import com.castori_de_munte.motivity.motivity_client.models.UserObj;

import java.util.Map;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.QueryMap;
import retrofit2.http.Url;

public interface MissionsService {
    @GET
    Call<ResponseList<MissionObj.Mission>> userMissions(
            @Url String url, @QueryMap Map<String, String> options
    );

    @GET
    Call<MissionObj> get(@Url String url);

    @POST
    Call<MissionObj> insert(@Url String url, @Body MissionObj missionObj);

    @DELETE//("user/{user_id}/mission/{id}")
    Call<String> delete(@Url String url);

    @PUT//("user/{user_id}/mission/{id}")
    Call<MissionObj> update(@Url String url, @Body MissionObj missionObj);

}
