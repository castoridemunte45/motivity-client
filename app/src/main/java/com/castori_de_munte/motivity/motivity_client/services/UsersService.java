package com.castori_de_munte.motivity.motivity_client.services;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;

import com.castori_de_munte.motivity.motivity_client.Global;
import com.castori_de_munte.motivity.motivity_client.R;
import com.castori_de_munte.motivity.motivity_client.SharedFunctions;
import com.castori_de_munte.motivity.motivity_client.middleware.ServiceCreator;
import com.castori_de_munte.motivity.motivity_client.models.UserObj;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UsersService {
    public HashMap<String, Boolean> actions;
    private Context context;
    private com.castori_de_munte.motivity.motivity_client.interfaces.UsersService usersService;
    private String eventName;

    public UsersService(Context context, String eventName){
        this.actions = new HashMap<>();
        this.context = context;
        this.eventName = eventName;
        this.usersService =
                ServiceCreator.retrofitService(com.castori_de_munte.motivity.motivity_client.interfaces.UsersService.class, context);
    }

    public void update(UserObj user){
        if ((actions.containsKey("update") && actions.get("update")) || SharedFunctions.checkNetworkStatus(context, usersService, eventName)) return;
        else
            actions.put("update", true);
        Call<UserObj> call = usersService.update(user, "update/" + Global.user.getId());
        call.enqueue(new Callback<UserObj>() {
            @Override
            public void onResponse(Call<UserObj> call, Response<UserObj> response) {
                int statusCode = response.code();
                actions.remove("update");
                Intent intent = new Intent(eventName);

                if(statusCode == 200) {
                    Global.user = response.body().getUser();
                }else {
                    String message = Global.isDev ?
                            response.message() : context.getResources().getString(R.string.error_generic);
                    intent.putExtra("error", message);
                }

                LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
            }

            @Override
            public void onFailure(Call<UserObj> call, Throwable t) {
                SharedFunctions.failing(context, t, eventName);
            }
        });
    }

    public void getMe(){
        if ((actions.containsKey("getMe") && actions.get("getMe")) || SharedFunctions.checkNetworkStatus(context, usersService, eventName)) return;
        else
            actions.put("getMe", true);
        Call<UserObj.User> call = usersService.getMe();
        call.enqueue(new Callback<UserObj.User>() {
            @Override
            public void onResponse(Call<UserObj.User> call, Response<UserObj.User> response) {
                int statusCode = response.code();
                actions.remove("getMe");
                Intent intent = new Intent(eventName);

                if(statusCode == 200) {
                    Global.user = response.body();
                }else {
                    String message = Global.isDev ?
                            response.message() : context.getResources().getString(R.string.error_generic);
                    intent.putExtra("error", message);
                }

                LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
            }

            @Override
            public void onFailure(Call<UserObj.User> call, Throwable t) {
                SharedFunctions.failing(context, t, eventName);
            }
        });
    }
}
