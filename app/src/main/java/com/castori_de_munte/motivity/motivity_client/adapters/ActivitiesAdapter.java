package com.castori_de_munte.motivity.motivity_client.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.ShapeDrawable;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.app.AlertDialog;
import android.content.DialogInterface;

import com.castori_de_munte.motivity.motivity_client.Global;
import com.castori_de_munte.motivity.motivity_client.R;
import com.castori_de_munte.motivity.motivity_client.SharedFunctions;
import com.castori_de_munte.motivity.motivity_client.services.ActivitiesService;
import com.castori_de_munte.motivity.motivity_client.models.ActivityObj;

import java.util.List;

public class ActivitiesAdapter extends RecyclerView.Adapter<ActivitiesAdapter.ViewHolder> {
    private LayoutInflater inflater;
    private List<ActivityObj.Activity> activities;
    private Context context;
    private ActivitiesService activitiesService;

    public ActivitiesAdapter(Context context, List<ActivityObj.Activity> activities){
        inflater = LayoutInflater.from(context);
        this.context = context;
        this.activities = activities;
        activitiesService =  new ActivitiesService(context.getApplicationContext(), "deleted-activity");
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.fragment_activity_view_holder, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(ActivitiesAdapter.ViewHolder holder, int position) {
        ActivityObj.Activity activity = activities.get(position);
        holder.tvTitle.setText(activity.getTitle());
        holder.tvDescription.setText(activity.getDescription());
        String expText = "Current: " + activity.getExperience() + " exp";
        holder.tvExperience.setText(expText);
        String nextLvlExp = "Next level: " + activity.getNextLevelExp() + " exp";
        holder.tvNextLevelExp.setText(nextLvlExp);
        holder.pbExperience.getProgressDrawable().setColorFilter(
                ContextCompat.getColor(context, R.color.green), android.graphics.PorterDuff.Mode.SRC_IN);
        SharedFunctions.setMax(holder.pbExperience, activity.getNextLevelExp(), 100);
        SharedFunctions.setProgress(holder.pbExperience, activity.getExperience(), 100);
        holder.id = activity.getId();
        holder.tvLevel.setText(String.valueOf(activity.getLevel()));
        int[] difficultyColors = context.getResources().getIntArray(R.array.difficulty);
        Drawable background = holder.imgView.getBackground();
        if (background instanceof ShapeDrawable) {
            ((ShapeDrawable)background).getPaint().setColor(difficultyColors[activity.getDifficulty() - 1]);
        } else if (background instanceof GradientDrawable) {
            ((GradientDrawable)background).setColor(difficultyColors[activity.getDifficulty() - 1]);
        } else if (background instanceof ColorDrawable) {
            ((ColorDrawable)background).setColor(difficultyColors[activity.getDifficulty() - 1]);
        }
    }

    @Override
    public int getItemCount() {
        return activities.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {
        private TextView tvTitle;
        private TextView tvDescription;
        private TextView tvExperience;
        private TextView tvNextLevelExp;
        private ProgressBar pbExperience;
        private TextView tvLevel;
        private TextView imgView;
        private int id;

        public ViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            itemView.setOnLongClickListener(this);
            tvTitle = (TextView) itemView.findViewById(R.id.tvTitle);
            tvDescription = (TextView) itemView.findViewById(R.id.tvDescription);
            tvExperience = (TextView) itemView.findViewById(R.id.tvExperience);
            tvNextLevelExp = (TextView) itemView.findViewById(R.id.tvNextLevel);
            pbExperience = (ProgressBar) itemView.findViewById(R.id.pbExperience);
            tvLevel = (TextView) itemView.findViewById(R.id.tvLevel);
            imgView = (TextView) itemView.findViewById(R.id.imgView);
        }


        @Override
        public void onClick(View view) {
            Intent intent = new Intent("selected-activity");
            intent.putExtra("activityId", id);
            intent.putExtra("title", tvTitle.getText().toString());
            intent.putExtra("description", tvDescription.getText().toString());
            intent.putExtra("experience", tvExperience.getText().toString());
            intent.putExtra("nextLevel", tvNextLevelExp.getText().toString());
            intent.putExtra("level", tvLevel.getText().toString());

            for(int i = 0; i < activities.size(); i++){
                if(activities.get(i).getId() == id){
                    Global.activityPosition = i;
                    break;
                }
            }
            LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
        }

        @Override
        public boolean onLongClick(View view) {
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setTitle("Delete activity");
            builder.setMessage("Are you sure you want to delete this activity?");
            builder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    activitiesService.delete(id);

                }
            });
            builder.setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                }
            });
            builder.setIcon(android.R.drawable.ic_dialog_alert);
            AlertDialog dialog = builder.create();
            dialog.show();

            return true;
        }

    }
}
