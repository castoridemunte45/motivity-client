package com.castori_de_munte.motivity.motivity_client.interfaces;

import com.castori_de_munte.motivity.motivity_client.models.ActivityObj;
import com.castori_de_munte.motivity.motivity_client.models.ResponseList;

import java.util.Map;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.QueryMap;
import retrofit2.http.Url;

public interface ActivitiesService {
    @GET
    Call<ResponseList<ActivityObj.Activity>> userActivities(
            @Url String url, @QueryMap Map<String, String> options
    );

    @GET//("user/{user_id}/activity/{id}")
    Call<ActivityObj> get(@Url String url);

    @PUT
    Call<ActivityObj> update(@Url String url, @Body ActivityObj activityObj);

    @POST
    Call<ActivityObj> insert(@Url String url, @Body ActivityObj activityObj);

    @DELETE
    Call<String> delete(@Url String url);

}
