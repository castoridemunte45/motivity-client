package com.castori_de_munte.motivity.motivity_client.models;


import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ResponseList<T> {
    @JsonCreator
    public ResponseList(){

    }

    @JsonProperty("items")
    private List<T> items;

    @JsonProperty("total")
    private int total;

    @JsonProperty("items")
    public List<T> getItems() {
        return items;
    }

    @JsonProperty("items")
    public void setItems(List<T> items) {
        this.items = items;
    }

    @JsonProperty("total")
    public int getTotal() {
        return total;
    }

    @JsonProperty("total")
    public void setTotal(int total) {
        this.total = total;
    }
}
