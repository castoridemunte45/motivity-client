package com.castori_de_munte.motivity.motivity_client.services;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;

import com.castori_de_munte.motivity.motivity_client.Global;
import com.castori_de_munte.motivity.motivity_client.R;
import com.castori_de_munte.motivity.motivity_client.SharedFunctions;
import com.castori_de_munte.motivity.motivity_client.middleware.ServiceCreator;
import com.castori_de_munte.motivity.motivity_client.models.MissionObj;
import com.castori_de_munte.motivity.motivity_client.models.ResponseList;
import com.castori_de_munte.motivity.motivity_client.models.UserAchievementObj;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by izabe on 1/8/2017.
 */

public class UserAchievementsService {
    public HashMap<String, Boolean> actions;
    private Context context;
    private com.castori_de_munte.motivity.motivity_client.interfaces.UserAchievementsService userAchievementsService;
    private String eventName;

    public UserAchievementsService(Context context, String eventName){
        this.actions = new HashMap<>();
        this.context = context;
        this.eventName = eventName;
        this.userAchievementsService =
                ServiceCreator.retrofitService(com.castori_de_munte.motivity.motivity_client.interfaces.UserAchievementsService.class, context);
    }

    public void list(Map<String, String> options){
        if ((actions.containsKey("list") && actions.get("list")) || SharedFunctions.checkNetworkStatus(context, userAchievementsService, eventName)) return;
        else
            actions.put("list", true);
        String url = "user/" + Global.user.getId() + "/achievement";
        Call<ResponseList<UserAchievementObj.UserAchievement>> call = userAchievementsService.userAchievements(url,  options);
        call.enqueue(new Callback<ResponseList<UserAchievementObj.UserAchievement>>() {
            @Override
            public void onResponse(Call<ResponseList<UserAchievementObj.UserAchievement>> call, Response<ResponseList<UserAchievementObj.UserAchievement>> response) {
                int statusCode = response.code();
                actions.remove("list");
                Intent intent = new Intent(eventName);

                if(statusCode == 200) {
                    List<UserAchievementObj.UserAchievement> userAchievements = response.body().getItems();
                    for(UserAchievementObj.UserAchievement userAchievement : userAchievements){
                        Global.userAchievements.add(userAchievement);
                    }
                }else {
                    String message = Global.isDev ?
                            response.message() : context.getResources().getString(R.string.error_generic);
                    intent.putExtra("error", message);
                }

                LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
            }

            @Override
            public void onFailure(Call<ResponseList<UserAchievementObj.UserAchievement>> call, Throwable t) {
                SharedFunctions.failing(context, t, eventName);
            }
        });
    }

    public void delete(int id){
        if ((actions.containsKey("delete") && actions.get("delete")) || SharedFunctions.checkNetworkStatus(context, userAchievementsService, eventName)) return;
        else
            actions.put("delete", true);
        String url = "user/" + Global.user.getId() + "/achievement/" + id;
        Call<String> call = userAchievementsService.delete(url);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                int statusCode = response.code();
                actions.remove("delete");
                Intent intent = new Intent(eventName);

                if(statusCode == 200) {
                    // For further implentation
                }else {
                    String message = Global.isDev ?
                            response.message() : context.getResources().getString(R.string.error_generic);
                    intent.putExtra("error", message);
                }

                LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                SharedFunctions.failing(context, t, eventName);
            }
        });
    }

    public void update(final UserAchievementObj userAchievementObj){
        if ((actions.containsKey("update") && actions.get("update")) || SharedFunctions.checkNetworkStatus(context, userAchievementsService, eventName)) return;
        else
            actions.put("update", true);
        String url = "user/" + Global.user.getId() + "/achievement/" + userAchievementObj.getUserAchievement().getId();
        Call<UserAchievementObj> call = userAchievementsService.update(url, userAchievementObj);
        call.enqueue(new Callback<UserAchievementObj>() {
            @Override
            public void onResponse(Call<UserAchievementObj> call, Response<UserAchievementObj> response) {
                int statusCode = response.code();
                actions.remove("delete");
                Intent intent = new Intent(eventName);

                if(statusCode == 200) {
                    Global.userAchievement = UserAchievementObj.UserAchievement.copyUserAchievement(response.body().getUserAchievement());
                    for(int i = 0; i < Global.missions.size(); i++){
                        if(Global.userAchievements.get(i).getId() == userAchievementObj.getUserAchievement().getId()){
                            Global.userAchievements.remove(i);
                            Global.userAchievements.add(i, UserAchievementObj.UserAchievement.copyUserAchievement(userAchievementObj.getUserAchievement()));
                        }
                    }
                }else {
                    String message = Global.isDev ?
                            response.message() : context.getResources().getString(R.string.error_generic);
                    intent.putExtra("error", message);
                }

                LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
            }

            @Override
            public void onFailure(Call<UserAchievementObj> call, Throwable t) {
                SharedFunctions.failing(context, t, eventName);
            }
        });
    }

    public void get(int id){
        if ((actions.containsKey("get") && actions.get("get")) || SharedFunctions.checkNetworkStatus(context, userAchievementsService, eventName)) return;
        else
            actions.put("get", true);
        Call<UserAchievementObj> call = userAchievementsService.get("user/" + Global.user.getId() + "/achievement/" + id);
        call.enqueue(new Callback<UserAchievementObj>() {
            @Override
            public void onResponse(Call<UserAchievementObj> call, Response<UserAchievementObj> response) {
                int statusCode = response.code();
                actions.remove("get");
                Intent intent = new Intent(eventName);

                if (statusCode == 200) {
                    Global.userAchievement = UserAchievementObj.UserAchievement.copyUserAchievement(response.body().getUserAchievement());
                }else {
                    String message = Global.isDev ?
                            response.message() : context.getResources().getString(R.string.error_generic);
                    intent.putExtra("error", message);
                }

                LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
            }
            public void onFailure(Call<UserAchievementObj> call, Throwable t) {
                SharedFunctions.failing(context, t, eventName);
            }
        });
    }

    public void insert(UserAchievementObj userAchievementObj){
        if ((actions.containsKey("insert") && actions.get("insert")) || SharedFunctions.checkNetworkStatus(context, userAchievementsService, eventName)) return;
        else
            actions.put("insert", true);
        String url = "user/" + Global.user.getId() + "/achievement";
        Call<UserAchievementObj> call = userAchievementsService.insert(url,userAchievementObj);
        call.enqueue(new Callback<UserAchievementObj>() {
            @Override
            public void onResponse(Call<UserAchievementObj> call, Response<UserAchievementObj> response) {
                int statusCode = response.code();
                actions.remove("insert");
                Intent intent = new Intent(eventName);

                if(statusCode == 200) {
                    Global.userAchievements.add(0, response.body().getUserAchievement());
                }else {
                    String message = Global.isDev ?
                            response.message() : context.getResources().getString(R.string.error_generic);
                    intent.putExtra("error", message);
                }

                LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
            }

            @Override
            public void onFailure(Call<UserAchievementObj> call, Throwable t) {
                SharedFunctions.failing(context, t, eventName);
            }
        });
    }
}
