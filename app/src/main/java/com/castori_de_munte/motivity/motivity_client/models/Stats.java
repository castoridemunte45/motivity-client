package com.castori_de_munte.motivity.motivity_client.models;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.Date;


@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Stats {
    @JsonCreator
    public Stats() {

    }

    @JsonProperty("friends")
    private int friends;

    @JsonProperty("activities")
    private int activities;

    @JsonProperty("completed_missions")
    private int completedMissions;

    @JsonProperty("friends")
    public int getFriends() {
        return friends;
    }

    @JsonProperty("friends")
    public void setFriends(int friends) {
        this.friends = friends;
    }

    @JsonProperty("activities")
    public int getActivities() {
        return activities;
    }

    @JsonProperty("activities")
    public void setActivities(int activities) {
        this.activities = activities;
    }

    @JsonProperty("completed_missions")
    public int getCompletedMissions() {
        return completedMissions;
    }

    @JsonProperty("completed_missions")
    public void setCompletedMissions(int completedMissions) {
        this.completedMissions = completedMissions;
    }
}

