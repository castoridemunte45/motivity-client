package com.castori_de_munte.motivity.motivity_client;


import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.design.widget.Snackbar;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import com.castori_de_munte.motivity.motivity_client.custom_controls.RandomString;
import com.castori_de_munte.motivity.motivity_client.custom_controls.SnackbarWrapper;
import com.castori_de_munte.motivity.motivity_client.services.AuthService;

public class RegisterActivity extends AppCompatActivity {

    private ProgressDialog progress;
    private EditText etFirstName;
    private EditText etLastName;
    private EditText etEmail;
    private EditText etPassword;
    private EditText etRepeatPassword;

    private BroadcastReceiver receivedRegister = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Bundle bundle = intent.getExtras();
            if(progress != null && progress.isShowing()) progress.dismiss();
            if(SharedFunctions.intentHasError(context.getApplicationContext(), bundle)){
                return;
            }
            handleRegister();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        etFirstName = (EditText) findViewById(R.id.etFirstName);
        etLastName = (EditText) findViewById(R.id.etLastName);
        etEmail = (EditText) findViewById(R.id.etEmail);
        etPassword = (EditText) findViewById(R.id.etPassword);
        etRepeatPassword = (EditText) findViewById(R.id.etRepeatPassword);

        if(Global.isDev) {
            RandomString rs = new RandomString(10);
            etFirstName.setText(rs.nextString());
            etLastName.setText(rs.nextString());
            etEmail.setText(rs.nextString() + "@motivity.com");
            String password = rs.nextString();
            etPassword.setText(password);
            etRepeatPassword.setText(password);
        }

        LocalBroadcastManager.getInstance(this).registerReceiver(receivedRegister,
                new IntentFilter("received-register"));
    }

    @Override
    public void onDestroy() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(receivedRegister);
        super.onDestroy();
    }

    public void register(View view) {
        boolean ok = true;

        if(! android.util.Patterns.EMAIL_ADDRESS.matcher(etEmail.getText().toString()).matches()) {
            etEmail.setError(getResources().getString(R.string.invalid_email));
            Snackbar.make(findViewById(R.id.drawer_layout_register), R.string.invalid_email, Snackbar.LENGTH_SHORT).show();
            ok = false;
        }

        if(etFirstName.getText().toString().isEmpty() || etLastName.getText().toString().isEmpty() || etEmail.getText().toString().isEmpty() ||
                etPassword.getText().toString().isEmpty() || etRepeatPassword.getText().toString().isEmpty()){
            if(etFirstName.getText().toString().isEmpty()) etFirstName.setError(getResources().getString(R.string.field_required));
            if(etLastName.getText().toString().isEmpty()) etLastName.setError(getResources().getString(R.string.field_required));
            if(etEmail.getText().toString().isEmpty()) etEmail.setError(getResources().getString(R.string.field_required));
            if(etPassword.getText().toString().isEmpty()) etPassword.setError(getResources().getString(R.string.field_required));
            if(etRepeatPassword.getText().toString().isEmpty()) etRepeatPassword.setError(getResources().getString(R.string.field_required));
            Snackbar.make(findViewById(R.id.drawer_layout_register), R.string.fields_empty, Snackbar.LENGTH_SHORT).show();
            ok = false;
        }

        if(!etPassword.getText().toString().equals(etRepeatPassword.getText().toString())){
            etRepeatPassword.setError(getResources().getString(R.string.passwords_mismatch));
            Snackbar.make(findViewById(R.id.drawer_layout_register), R.string.passwords_mismatch, Snackbar.LENGTH_SHORT).show();
            ok = false;
        }

        if (!ok) return;

        AuthService authService = new AuthService(getApplicationContext(), "received-register");
        authService.register(etFirstName.getText().toString(), etLastName.getText().toString(),
                etEmail.getText().toString(), etPassword.getText().toString());
    }

    private void handleRegister(){
        Intent loginActivity = new Intent(RegisterActivity.this, LoginActivity.class);
        loginActivity.putExtra("email", etEmail.getText().toString());
        loginActivity.putExtra("password", etPassword.getText().toString());
        startActivity(loginActivity);
    }
}