package com.castori_de_munte.motivity.motivity_client.custom_controls;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.design.widget.Snackbar;


public class NetworkStatus {
    private static NetworkStatus instance = new NetworkStatus();
    static Context context;

    public static NetworkStatus getInstance(Context ctx) {
        context = ctx.getApplicationContext();
        return instance;
    }

    public boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        boolean connected = activeNetworkInfo != null && activeNetworkInfo.isConnected();
//        if(!connected){
//            SnackbarWrapper snackbarWrapper = SnackbarWrapper.make(context.getApplicationContext(),
//                    context.getResources().getString(R.string.no_internet), Snackbar.LENGTH_SHORT);
//            snackbarWrapper.show();
//        }
        return connected;
    }
}
