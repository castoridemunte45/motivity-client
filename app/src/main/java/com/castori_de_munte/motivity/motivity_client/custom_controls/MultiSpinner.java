package com.castori_de_munte.motivity.motivity_client.custom_controls;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.util.AttributeSet;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import java.util.ArrayList;
import java.util.List;

public class MultiSpinner<T> extends Spinner implements
        DialogInterface.OnMultiChoiceClickListener, DialogInterface.OnCancelListener {

    private List<T> items;
    private boolean[] selected;
    private List<Integer> history;
    private String none, all;
    private MultiSpinnerListener listener;
    private int limit;

    private int currNo;

    public MultiSpinner(Context context) {
        super(context);
    }

    public MultiSpinner(Context arg0, AttributeSet arg1) {
        super(arg0, arg1);
    }

    public MultiSpinner(Context arg0, AttributeSet arg1, int arg2) {
        super(arg0, arg1, arg2);
    }

    @Override
    public void onClick(DialogInterface dialog, int which, boolean isChecked) {
        if (isChecked) {
            history.add(0, which);
            selected[which] = true;
            currNo ++;
            if(limit > 0 && currNo > limit){
                currNo --;
                ((AlertDialog) dialog).getListView().setItemChecked(history.get(history.size() - 1), false);
                selected[history.get(history.size() - 1)] = false;
                history.remove(history.get(history.size() - 1));
            }
        }else {
            selected[which] = false;
            currNo --;
            for(int i = 0; i < history.size(); i++){
                if(history.get(i) == which){
                    history.remove(history.get(i));
                    break;
                }
            }
        }
    }

    @Override
    public void onCancel(DialogInterface dialog) {
        // refresh text on spinner
        StringBuilder spinnerBuffer = new StringBuilder();
        boolean someUnselected = false, someSelected = false;
        for (int i = 0; i < items.size(); i++) {
            if (selected[i]) {
                spinnerBuffer.append(items.get(i).toString());
                spinnerBuffer.append(", ");
                someSelected = true;
            } else {
                someUnselected = true;
            }
        }
        String spinnerText;
        if (someUnselected && someSelected) {
            spinnerText = spinnerBuffer.toString();
            if (spinnerText.length() > 2)
                spinnerText = spinnerText.substring(0, spinnerText.length() - 2);
        } else
        if (!someUnselected && someSelected){
            spinnerText = all;
        }else{
            spinnerText = none;
        }
        ArrayAdapter<String> adapter = new ArrayAdapter<>(getContext(),
                android.R.layout.simple_spinner_item,
                new String[] { spinnerText });
        setAdapter(adapter);
        listener.onItemsSelected(selected);
    }

    @Override
    public boolean performClick() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        List<String> stringItems = new ArrayList<>();
        for(T item : items){
            stringItems.add(item.toString());
        }
        builder.setMultiChoiceItems(
                stringItems.toArray(new CharSequence[items.size()]), selected, this);
        builder.setPositiveButton(android.R.string.ok,
                new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
        builder.setOnCancelListener(this);
        builder.show();
        return true;
    }

    public void setItems(List<T> items, String all, String none, int limit,
                         MultiSpinnerListener listener) {
        this.items = items;
        this.none = none;
        this.all = all;
        this.limit = limit;
        this.listener = listener;

        currNo = 0;
        selected = new boolean[items.size()];
        history = new ArrayList<>();

        ArrayAdapter<String> adapter = new ArrayAdapter<>(getContext(),
                android.R.layout.simple_spinner_item, new String[] { none });
        setAdapter(adapter);
    }

    public interface MultiSpinnerListener {
        public void onItemsSelected(boolean[] selected);
    }
}