package com.castori_de_munte.motivity.motivity_client.fragments;


import android.app.FragmentTransaction;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.app.Fragment;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.Toast;

import com.castori_de_munte.motivity.motivity_client.Global;
import com.castori_de_munte.motivity.motivity_client.R;
import com.castori_de_munte.motivity.motivity_client.SharedFunctions;
import com.castori_de_munte.motivity.motivity_client.adapters.ActivitiesAdapter;
import com.castori_de_munte.motivity.motivity_client.custom_controls.EndlessScrollListener;
import com.castori_de_munte.motivity.motivity_client.services.ActivitiesService;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class Activities extends Fragment {

    private LinearLayoutManager llmRecycleLayout;
    private RecyclerView rvRecycler;
    private FloatingActionButton fab;
    private FrameLayout flFallback;
    private SwipeRefreshLayout srlSwipeRefresh;

    private ActivitiesService activitiesService;
    private ActivitiesAdapter activitiesAdapter;

    private BroadcastReceiver receivedActivities = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Bundle bundle = intent.getExtras();
            if(SharedFunctions.intentHasError(context.getApplicationContext(), bundle)){
                return;
            }
            showActivities();
        }
    };

    private BroadcastReceiver deletedActivity = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Bundle bundle = intent.getExtras();
            if(SharedFunctions.intentHasError(context.getApplicationContext(), bundle)){
                return;
            }
            showActivities();
        }
    };

    private BroadcastReceiver selectedActivity = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Bundle bundle = intent.getExtras();
            if(SharedFunctions.intentHasError(context.getApplicationContext(), bundle)){
                return;
            }
            Intent viewActivityIntent = new Intent("change-fragment");
            viewActivityIntent.putExtra("fragmentType", "view-activity");
            viewActivityIntent.putExtras(bundle);
            LocalBroadcastManager.getInstance(context).sendBroadcast(viewActivityIntent);
        }
    };


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(receivedActivities,
                new IntentFilter("received-activities"));
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(selectedActivity,
                new IntentFilter("selected-activity"));
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(deletedActivity,
                new IntentFilter("deleted-activity"));
        return inflater.inflate(R.layout.fragment_activities, container, false);
    }

    @Override
    public void onDestroy() {
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(receivedActivities);
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(selectedActivity);
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(deletedActivity);
        super.onDestroy();
    }
    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initUI();

        activitiesService = new ActivitiesService(getActivity().getApplicationContext(), "received-activities");
        if(Global.activities.size() == 0){
            activitiesService.list(Global.defaultOptions);
        }else{
            showActivities();
            rvRecycler.scrollToPosition(Global.activityPosition);
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.toolbar, menu);
        initClearFilters(menu);
        initSearchView(menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    private void initClearFilters(final Menu menu){
        final MenuItem clearFilters = menu.findItem(R.id.action_clear_filters);
        clearFilters.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                inputSearchQuery(menu, "");
                Global.activitiesOptions.remove("keyword");
                srlSwipeRefresh.setRefreshing(true);
                rvRecycler.setVisibility(View.GONE);
                Map<String, String> options = new HashMap<>(Global.defaultOptions);
                searchActivities(options);
                return true;
            }
        });
    }

    private void inputSearchQuery(Menu menu, String query){
        final MenuItem searchMenu = menu.findItem(R.id.action_search);
        final SearchView searchView = (SearchView) searchMenu.getActionView();

        if(query.isEmpty()){
            searchMenu.collapseActionView();
        }else {
            searchMenu.expandActionView();
        }
        searchView.setIconified(false);
        searchView.setQuery(query, false);
        searchView.clearFocus();
    }

    private void initSearchView(Menu menu){
        final MenuItem searchMenu = menu.findItem(R.id.action_search);
        final SearchView searchView = (SearchView) searchMenu.getActionView();

        searchView.setIconifiedByDefault(false);
//        if(Global.options.containsKey("keyword") && !Global.options.get("keyword").isEmpty()) {
//            inputSearchQuery(menu, Global.options.get("keyword"));
//        }

        ImageView closeButton = (ImageView) searchView.findViewById(R.id.search_close_btn);
        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText et = (EditText) searchView.findViewById(R.id.search_src_text);
                et.setText("");
                searchView.setQuery("", false);
                searchView.onActionViewCollapsed();
                searchMenu.collapseActionView();
                searchView.clearFocus();
                Map<String, String> options = new HashMap<>(Global.defaultOptions);
                options.put("keyword", "");
                searchActivities(options);
            }
        });
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                searchView.clearFocus();
                Map<String, String> options = new HashMap<>(Global.defaultOptions);
                if(!query.isEmpty())
                    options.put("keyword", query);
                searchActivities(options);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                return false;
            }
        });
    }

    private void searchActivities(Map<String, String> attrs){
        Global.activities.clear();
        Global.activityPosition = -1;
        setScrollListeners();
        ActivitiesService activitiesService =
                new ActivitiesService(getActivity().getApplicationContext(), "received-activities");

        if(attrs != null) {
            Iterator it = attrs.entrySet().iterator();
            while (it.hasNext()) {
                Map.Entry pair = (Map.Entry) it.next();
                String key = (String) pair.getKey(), value = (String) pair.getValue();
                Global.activitiesOptions.put(key, value);
                it.remove(); // avoids a ConcurrentModificationException
            }
        }else{
            Global.activitiesOptions = new HashMap<>(Global.defaultOptions);
        }
        activitiesService.list(Global.activitiesOptions);
    }

    private void initUI(){
        View view = getView();
        if(view != null) {
            llmRecycleLayout = new LinearLayoutManager(getActivity());
            flFallback = (FrameLayout) view.findViewById(R.id.fallback_frame);
            rvRecycler = (RecyclerView) view.findViewById(R.id.recycler_view_activities);
            srlSwipeRefresh = (SwipeRefreshLayout) view.findViewById(R.id.swipe_refresh);
            fab = (FloatingActionButton) getActivity().findViewById(R.id.fab);
            if(fab != null){
                if (!fab.isShown()) fab.show();
            }

            setScrollListeners();

            srlSwipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                @Override
                public void onRefresh() {
                    Global.activities.clear();
                    Global.activitiesOptions.put("page", Global.defaultOptions.get("page"));
                    rvRecycler.setVisibility(View.GONE);
                    activitiesService.list(Global.activitiesOptions);
                    setScrollListeners();
                }
            });

            FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
            Loading loading = new Loading();
            fragmentTransaction.replace(R.id.fallback_frame, loading);
            fragmentTransaction.commitAllowingStateLoss();
        }
    }

    private void setScrollListeners(){
        rvRecycler.clearOnScrollListeners();
        rvRecycler.addOnScrollListener(new EndlessScrollListener(llmRecycleLayout) {
            @Override
            public void onLoadMore(int page, int totalItemsCount) {
                if(Integer.parseInt(Global.activitiesOptions.get("page")) > page) return;
                Global.activitiesOptions.put("page", String.valueOf(page));
                activitiesService.list(Global.activitiesOptions);
            }
        });
        rvRecycler.addOnScrollListener(new RecyclerView.OnScrollListener(){
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy){
                if (dy > 0 || dy<0 && fab.isShown())
                    fab.hide();
            }

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {

                if (newState == AbsListView.OnScrollListener.SCROLL_STATE_IDLE){
                    fab.show();
                }
                super.onScrollStateChanged(recyclerView, newState);
            }
        });
    }

    private void showActivities(){
        if(srlSwipeRefresh.isRefreshing()){
            srlSwipeRefresh.setRefreshing(false);
        }

        flFallback.setVisibility(View.GONE);
        rvRecycler.setVisibility(View.VISIBLE);
        if(activitiesAdapter == null) {
            activitiesAdapter = new ActivitiesAdapter(getActivity(), Global.activities);
            rvRecycler.setAdapter(activitiesAdapter);
            rvRecycler.setLayoutManager(llmRecycleLayout);
        }else{
            activitiesAdapter.notifyDataSetChanged();
        }
    }
}
