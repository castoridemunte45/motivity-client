package com.castori_de_munte.motivity.motivity_client.fragments;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.ShapeDrawable;
import android.os.Bundle;
import android.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.TextView;

import com.castori_de_munte.motivity.motivity_client.Global;
import com.castori_de_munte.motivity.motivity_client.R;
import com.castori_de_munte.motivity.motivity_client.SharedFunctions;
import com.castori_de_munte.motivity.motivity_client.custom_controls.RandomString;
import com.castori_de_munte.motivity.motivity_client.models.ActivityObj;
import com.castori_de_munte.motivity.motivity_client.services.ActivitiesService;

public class CreateActivity extends Fragment {

    private EditText etTitle;
    private EditText etDescription;
    private TextView tvDifficulty;
    private SeekBar sbDifficulty;
    private TextView imgView;

    private Button submitButton;

    private ActivitiesService activitiesService;

    private BroadcastReceiver activityCreated = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Bundle bundle = intent.getExtras();
            if(SharedFunctions.intentHasError(context.getApplicationContext(), bundle)){
                    return;
            }
            Intent viewActivityIntent = new Intent("change-fragment");
            viewActivityIntent.putExtra("fragmentType", "user-activities");
            //if(bundle!=null) {
               // viewActivityIntent.putExtras(bundle);
            //}
            LocalBroadcastManager.getInstance(context).sendBroadcast(viewActivityIntent);
        }
    };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(activityCreated,
                new IntentFilter("activity-created"));
        return inflater.inflate(R.layout.fragment_create_activity, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        activitiesService = new ActivitiesService(getActivity().getApplicationContext(), "activity-created");
        initUI();
    }

    @Override
    public void onDestroy() {
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(activityCreated);
        super.onDestroy();
    }

    private void initUI(){
        final View view = getView();
        if(view != null) {
            submitButton = (Button) getActivity().findViewById(R.id.button_create_activity);

            etTitle = (EditText) view.findViewById(R.id.etTitle);
            etDescription = (EditText) view.findViewById(R.id.etDescription);
            tvDifficulty = (TextView) view.findViewById(R.id.tvDifficulty);
            sbDifficulty = (SeekBar) view.findViewById(R.id.seekbar);
            imgView = (TextView) view.findViewById(R.id.imgView);
            final int[] difficultyColors = this.getResources().getIntArray(R.array.difficulty);
            final Drawable background = imgView.getBackground();
            ((GradientDrawable)background).setColor(difficultyColors[0]);
            tvDifficulty.setText(Global.difficulties.get(0));
            sbDifficulty.setMax(Global.difficulties.size() - 1);
            sbDifficulty.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                @Override
                public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                    tvDifficulty.setText(Global.difficulties.get(i));
                    if (background instanceof ShapeDrawable) {
                        ((ShapeDrawable)background).getPaint().setColor(difficultyColors[i]);
                    } else if (background instanceof GradientDrawable) {
                        ((GradientDrawable)background).setColor(difficultyColors[i]);
                    } else if (background instanceof ColorDrawable) {
                        ((ColorDrawable)background).setColor(difficultyColors[i]);
                    }
                }

                @Override
                public void onStartTrackingTouch(SeekBar seekBar) {

                }

                @Override
                public void onStopTrackingTouch(SeekBar seekBar) {

                }
            });

            if(Global.isDev){
                RandomString rs = new RandomString(10);
                etTitle.setText(rs.nextString());
                etDescription.setText(rs.nextString());
            }

            submitButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                ActivityObj activityObj = new ActivityObj();
                activityObj.getActivity().setTitle(etTitle.getText().toString());
                activityObj.getActivity().setDescription(etDescription.getText().toString());
                activityObj.getActivity().setDifficulty(sbDifficulty.getProgress() + 1);
                activityObj.getActivity().setUserId(Global.user.getId());

                activitiesService.insert(activityObj, true);
                }
            });
        }
    }

}
