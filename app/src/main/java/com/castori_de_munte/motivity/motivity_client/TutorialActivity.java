package com.castori_de_munte.motivity.motivity_client;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.castori_de_munte.motivity.motivity_client.custom_controls.NetworkStatus;
import com.castori_de_munte.motivity.motivity_client.models.UserObj;
import com.castori_de_munte.motivity.motivity_client.services.UsersService;

/**
 * Created by izabe on 1/7/2017.
 */

public class TutorialActivity extends AppCompatActivity {
    private Button btNextImg;
    private Button btSkipTutorial;
    private ImageView ivTutorial;
    private int index = 0;

    private BroadcastReceiver receivedTutorial = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Bundle bundle = intent.getExtras();
            if(SharedFunctions.intentHasError(context.getApplicationContext(), bundle)){
                return;
            }
            Intent homeActivity = new Intent(TutorialActivity.this, HomeActivity.class);
            homeActivity.putExtra("fragmentType", "active-missions");
            startActivity(homeActivity);
        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tutorial);
        LocalBroadcastManager.getInstance(this).registerReceiver(receivedTutorial,
                new IntentFilter("tutorial"));
        addListenerOnButtonNext();
        addListenerOnButtonSkip();
    }

    @Override
    public void onDestroy() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(receivedTutorial);
        super.onDestroy();
    }

    public void redirectToHomePage(){
        Global.user.setSeenTutorial(true);
        UserObj userObj = new UserObj();
        userObj.setUser(Global.user);
        UsersService userService = new UsersService(getApplicationContext(), "tutorial");
        userService.update(userObj);
    }

    private void addListenerOnButtonSkip() {
        btSkipTutorial = (Button) findViewById(R.id.btSkipTutorial);
        btSkipTutorial.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                redirectToHomePage();
            }
        });
    }

    private void addListenerOnButtonNext() {
        ivTutorial = (ImageView) findViewById(R.id.ivTutorial);
        btNextImg = (Button) findViewById(R.id.btNextImg);
        btNextImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                index++;
                switch(index){
                    case 1:
                        ivTutorial.setImageResource(R.drawable.p1);
                        break;
                    case 2:
                        ivTutorial.setImageResource(R.drawable.p2);
                        break;
                    case 3:
                        ivTutorial.setImageResource(R.drawable.p3);
                        break;
                    case 4:
                        ivTutorial.setImageResource(R.drawable.p4);
                        break;
                    case 5:
                        ivTutorial.setImageResource(R.drawable.p5);
                        break;
                    case 6:
                        ivTutorial.setImageResource(R.drawable.p6);
                        break;
                    case 7:
                        ivTutorial.setImageResource(R.drawable.p7);
                        break;
                    case 8:
                        ivTutorial.setImageResource(R.drawable.p8);
                        break;
                    case 9:
                        ivTutorial.setImageResource(R.drawable.p9);
                        break;
                    case 10:
                        ivTutorial.setImageResource(R.drawable.p10);
                        break;
                    case 11:
                        ivTutorial.setImageResource(R.drawable.p11);
                        break;
                    case 12:
                        ivTutorial.setImageResource(R.drawable.p12);
                        break;
                    case 13:
                        ivTutorial.setImageResource(R.drawable.p13);
                        break;
                    case 14:
                        ivTutorial.setImageResource(R.drawable.p14);
                        break;
                    case 15:
                        ivTutorial.setImageResource(R.drawable.p15);
                        break;
                    case 16:
                        ivTutorial.setImageResource(R.drawable.p16);
                        btNextImg.setText("Finish");
                        break;
                    default:
                        redirectToHomePage();
                }

            }
        });
    }
}
