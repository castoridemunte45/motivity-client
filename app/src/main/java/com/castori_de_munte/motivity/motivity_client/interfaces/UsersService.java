package com.castori_de_munte.motivity.motivity_client.interfaces;

import com.castori_de_munte.motivity.motivity_client.models.ResponseList;
import com.castori_de_munte.motivity.motivity_client.models.UserObj;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Url;

public interface UsersService {
    @GET("user")
    Call<ResponseList<UserObj.User>> allUsers();

    @POST("user")
    Call<UserObj> insert(@Body UserObj user);

    @GET("auth/me")
    Call<UserObj.User> getMe();

    @PUT
    Call<UserObj> update(@Body UserObj user, @Url String url);
}
