package com.castori_de_munte.motivity.motivity_client.interfaces;

import com.castori_de_munte.motivity.motivity_client.models.ActivityObj;
import com.castori_de_munte.motivity.motivity_client.models.ResponseList;
import com.castori_de_munte.motivity.motivity_client.models.Stats;
import com.castori_de_munte.motivity.motivity_client.models.UserAchievementObj;
import com.castori_de_munte.motivity.motivity_client.models.UserObj;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Url;

public interface AchievementsService {
    @GET
    Call<Stats> getStats(@Url String url);

    @PUT
    Call<UserAchievementObj> update(@Body UserAchievementObj userAchievement, @Url String url);
}
