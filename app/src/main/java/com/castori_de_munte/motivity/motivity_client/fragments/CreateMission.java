package com.castori_de_munte.motivity.motivity_client.fragments;

import android.app.FragmentTransaction;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.opengl.Visibility;
import android.os.Bundle;
import android.app.Fragment;
import android.support.design.widget.Snackbar;
import android.support.v4.content.LocalBroadcastManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ProgressBar;

import com.castori_de_munte.motivity.motivity_client.Global;
import com.castori_de_munte.motivity.motivity_client.R;
import com.castori_de_munte.motivity.motivity_client.SharedFunctions;
import com.castori_de_munte.motivity.motivity_client.custom_controls.MultiSpinner;
import com.castori_de_munte.motivity.motivity_client.custom_controls.RandomString;
import com.castori_de_munte.motivity.motivity_client.custom_controls.SnackbarWrapper;
import com.castori_de_munte.motivity.motivity_client.models.ActivityObj;
import com.castori_de_munte.motivity.motivity_client.models.MissionObj;
import com.castori_de_munte.motivity.motivity_client.services.ActivitiesService;
import com.castori_de_munte.motivity.motivity_client.services.MissionsService;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class CreateMission extends Fragment {
    private FrameLayout flFallback;
    private EditText etTitle;
    private EditText etDuration;
    private EditText etDescription;
    private EditText etMilestone;
    private MultiSpinner<ActivityObj.Activity> multiSpinner;
    private Button createButton;

    private List<Integer> activityIds = new ArrayList<>();

    private MissionsService missionsService;
    private ActivitiesService activitiesService;

    private BroadcastReceiver receivedActivities = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Bundle bundle = intent.getExtras();
            if(SharedFunctions.intentHasError(context.getApplicationContext(), bundle)){
                return;
            }
            multiSpinnerSetItems();
        }
    };

    private BroadcastReceiver missionCreated = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Bundle bundle = intent.getExtras();
            if(SharedFunctions.intentHasError(context.getApplicationContext(), bundle)){
                return;
            }
            Intent viewMissionIntent = new Intent("change-fragment");
            viewMissionIntent.putExtra("fragmentType", "active-missions");
            LocalBroadcastManager.getInstance(context).sendBroadcast(viewMissionIntent);
        }
    };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(missionCreated,
                new IntentFilter("mission-created"));
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(receivedActivities,
                new IntentFilter("received-activities"));
        return inflater.inflate(R.layout.fragment_create_mission, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initUI();

        missionsService = new MissionsService(getActivity().getApplicationContext(), "mission-created");
        activitiesService = new ActivitiesService(getActivity().getApplicationContext(), "received-activities");
        Global.activities.clear();
        activitiesService.list(new HashMap<String, String>());
    }

    @Override
    public void onDestroy() {
        Global.activities.clear();
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(missionCreated);
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(receivedActivities);
        super.onDestroy();
    }

    private void initUI(){
        final View view = getView();
        if(view != null) {
            createButton = (Button) view.findViewById(R.id.btCreateMission);
            flFallback = (FrameLayout) view.findViewById(R.id.fallback_frame);
            etTitle = (EditText) view.findViewById(R.id.etTitle);
            etDuration = (EditText) view.findViewById(R.id.etDuration);
            etDescription = (EditText) view.findViewById(R.id.etDescription);
            etMilestone = (EditText) view.findViewById(R.id.etMilestone);

            FragmentTransaction transaction = getFragmentManager().beginTransaction();
            transaction.add(R.id.fallback_frame, new Loading());
            transaction.commit();

            if(Global.isDev){
                RandomString rs = new RandomString(10);
                etTitle.setText(rs.nextString());
                etDescription.setText(rs.nextString());
                etDuration.setText(String.valueOf(15));
                etMilestone.setText(String.valueOf(5));
            }

            multiSpinner = (MultiSpinner<ActivityObj.Activity>) view.findViewById(R.id.multi_spinner);

            createButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (activityIds.size() == 0){
                        SnackbarWrapper snackbarWrapper = SnackbarWrapper.make(getView().getContext(),
                                getView().getContext().getString(R.string.activities_required),
                                Snackbar.LENGTH_SHORT);
                        snackbarWrapper.show();
                        return;
                    }

                    MissionObj missionObj = new MissionObj();
                    missionObj.getMission().setTitle(etTitle.getText().toString());
                    missionObj.getMission().setDuration(Integer.parseInt(etDuration.getText().toString()));
                    missionObj.getMission().setDescription(etDescription.getText().toString());
                    missionObj.getMission().setMilestone(Integer.parseInt(etMilestone.getText().toString()));
                    missionObj.getMission().setActivityIds(activityIds);

                    missionsService.insert(missionObj);
                }
            });
        }
    }

    private void multiSpinnerSetItems(){
        flFallback.setVisibility(View.GONE);
        multiSpinner.setVisibility(View.VISIBLE);
        multiSpinner.setItems(Global.activities, getView().getResources().getString(R.string.all),
                getView().getResources().getString(R.string.none), Global.maxActivities,
                new MultiSpinner.MultiSpinnerListener() {
                    @Override
                    public void onItemsSelected(boolean[] selected) {
                        activityIds = new ArrayList<Integer>();
                        for(int i = 0; i < selected.length; i ++){
                            if(selected[i]){
                                activityIds.add(Global.activities.get(i).getId());
                            }
                        }
                    }
                }
        );
    }
}
