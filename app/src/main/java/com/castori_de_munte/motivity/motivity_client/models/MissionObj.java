package com.castori_de_munte.motivity.motivity_client.models;

import com.castori_de_munte.motivity.motivity_client.custom_controls.ISO8601;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class MissionObj {
    @JsonCreator
    public MissionObj(){
        this.mission = new Mission();
    }

    @JsonProperty("mission")
    private Mission mission;

    @JsonProperty("mission")
    public Mission getMission() {
        return mission;
    }

    @JsonProperty("mission")
    public void setMission(Mission mission) {
        this.mission = mission;
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class Mission{
        @JsonCreator
        public Mission() {
            this.duration = 1;
            this.progress = 0;
            this.completed = false;
            this.milestone = 1;
        }

        @JsonProperty("id")
        private int id;

        @JsonProperty("user_id")
        private int userId;

        @JsonProperty("title")
        private String title;

        @JsonProperty("description")
        private String description;

        @JsonProperty("completed")
        private boolean completed;

        @JsonProperty("expired")
        private boolean expired;

        @JsonProperty("milestone")
        private int milestone;

        @JsonProperty("progress")
        private int progress;

        @JsonProperty("duration")
        private int duration;

        @JsonProperty("created_at")
        private String createdAt;

        @JsonProperty("user")
        private UserObj.User user;

        @JsonProperty("activities")
        private List<ActivityObj.Activity> activities;

        @JsonProperty("activityIds")
        private List<Integer> activityIds;

        @JsonProperty("id")
        public int getId() {
            return id;
        }

        @JsonProperty("id")
        public void setId(int id) {
            this.id = id;
        }

        @JsonProperty("user_id")
        public int getUserId() {
            return userId;
        }

        @JsonProperty("user_id")
        public void setUserId(int userId) {
            this.userId = userId;
        }

        @JsonProperty("title")
        public String getTitle() {
            return title;
        }

        @JsonProperty("title")
        public void setTitle(String title) {
            this.title = title;
        }

        @JsonProperty("description")
        public String getDescription() {
            return description;
        }

        @JsonProperty("description")
        public void setDescription(String description) {
            this.description = description;
        }

        @JsonProperty("expired")
        public boolean getIsExpired() {
            return expired;
        }

        @JsonProperty("expired")
        public void setExpired(boolean expired) {
            this.expired = expired;
        }

        @JsonProperty("completed")
        public boolean getIsCompleted() {
            return completed;
        }

        @JsonProperty("completed")
        public void setCompleted(boolean completed) {
            this.completed = completed;
        }

        @JsonProperty("milestone")
        public int getMilestone() {
            return milestone;
        }

        @JsonProperty("milestone")
        public void setMilestone(int milestone) {
            this.milestone = milestone;
        }

        @JsonProperty("progress")
        public int getProgress() {
            return progress;
        }

        @JsonProperty("progress")
        public void setProgress(int progress) {
            this.progress = progress;
        }

        @JsonProperty("duration")
        public int getDuration() {
            return duration;
        }

        @JsonProperty("duration")
        public void setDuration(int duration) {
            this.duration = duration;
        }

        @JsonProperty("created_at")
        public String getCreatedAt() {
            return createdAt;
        }

        @JsonProperty("created_at")
        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        @JsonProperty("user")
        public UserObj.User getUser() {
            return user;
        }

        @JsonProperty("user")
        public void setUser(UserObj.User user) {
            this.user = user;
        }

        @JsonProperty("activities")
        public List<ActivityObj.Activity> getActivities() {
            return activities;
        }

        @JsonProperty("activities")
        public void setActivities(List<ActivityObj.Activity> activities) {
            this.activities = activities;
        }

        @JsonProperty("activityIds")
        public List<Integer> getActivityIds() {
            return activityIds;
        }

        @JsonProperty("activityIds")
        public void setActivityIds(List<Integer> activityIds) {
            this.activityIds = activityIds;
        }

        public static Mission copyMission(Mission mission){
            Mission newMission = new Mission();
            newMission.setId(mission.getId());
            newMission.setTitle(mission.getTitle());
            newMission.setDescription(mission.getDescription());
            newMission.setUserId(mission.getUserId());
            newMission.setCompleted(mission.getIsCompleted());
            newMission.setDuration(mission.getDuration());
            newMission.setMilestone(mission.getMilestone());
            newMission.setProgress(mission.getProgress());
            newMission.setActivities(mission.getActivities());
            newMission.setCreatedAt(mission.getCreatedAt());
            return newMission;
        }

        public static String getCreatedAtAsString(Mission mission){
            if(mission.getCreatedAt() == null || mission.getCreatedAt().equals("")) return "";
            Calendar date = ISO8601.toCalendar(mission.getCreatedAt());
            SimpleDateFormat sdf = new SimpleDateFormat("EEE MMM dd HH:mm:ss zzz yyyy", Locale.ROOT);
            if(date == null) return null;
            Date d = date.getTime();
            String aux[] = sdf.format(d).split(" ");
            return aux[3] + " " + aux[2] + "/" + aux[1] + "/" + aux[5] + "  ";
        }
    }
}
