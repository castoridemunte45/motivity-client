package com.castori_de_munte.motivity.motivity_client.models;

import com.castori_de_munte.motivity.motivity_client.SharedFunctions;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ActivityObj {
    @JsonCreator
    public ActivityObj(){
        this.activity = new Activity();
    }

    @JsonProperty("activity")
    private Activity activity;

    @JsonProperty("activity")
    public Activity getActivity() {
        return activity;
    }

    @JsonProperty("activity")
    public void setActivity(Activity activity) {
        this.activity = activity;
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class Activity {
        @JsonCreator
        public Activity() {
            this.difficulty = 1;
            this.experience = 0;
            this.level = 1;
            this.nextLevelExp = SharedFunctions.calculateNextLevelExp(this);
        }

        @JsonProperty("id")
        private int id;

        @JsonProperty("user_id")
        private int userId;

        @JsonProperty("title")
        private String title;

        @JsonProperty("description")
        private String description;

        @JsonProperty("difficulty")
        private int difficulty;

        @JsonProperty("experience")
        private int experience;

        @JsonProperty("next_level_exp")
        private int nextLevelExp;

        @JsonProperty("level")
        private int level;

        @JsonProperty("user")
        private UserObj.User user;

        @JsonProperty("id")
        public int getId() {
            return id;
        }

        @JsonProperty("id")
        public void setId(int id) {
            this.id = id;
        }

        @JsonProperty("user_id")
        public int getUserId() {
            return userId;
        }

        @JsonProperty("user_id")
        public void setUserId(int userId) {
            this.userId = userId;
        }

        @JsonProperty("title")
        public String getTitle() {
            return title;
        }

        @JsonProperty("title")
        public void setTitle(String title) {
            this.title = title;
        }

        @JsonProperty("description")
        public String getDescription() {
            return description;
        }

        @JsonProperty("description")
        public void setDescription(String description) {
            this.description = description;
        }

        @JsonProperty("difficulty")
        public int getDifficulty() {
            return difficulty;
        }

        @JsonProperty("difficulty")
        public void setDifficulty(int difficulty) {
            this.difficulty = difficulty;
        }

        @JsonProperty("experience")
        public int getExperience() {
            return experience;
        }

        @JsonProperty("experience")
        public void setExperience(int experience) {
            this.experience = experience;
        }

        @JsonProperty("next_level_exp")
        public int getNextLevelExp() {
            return nextLevelExp;
        }

        @JsonProperty("next_level_exp")
        public void setNextLevelExp(int nextLevelExp) {
            this.nextLevelExp = nextLevelExp;
        }

        @JsonProperty("level")
        public int getLevel() {
            return level;
        }

        @JsonProperty("level")
        public void setLevel(int level) {
            this.level = level;
        }

        @JsonProperty("user")
        public UserObj.User getUser() {
            return user;
        }

        @JsonProperty("user")
        public void setUser(UserObj.User user) {
            this.user = user;
        }

        @Override
        public String toString() {
            return this.title;
        }

        public static Activity copyActivity(Activity activity){
            Activity newActivity = new Activity();
            newActivity.setId(activity.getId());
            newActivity.setTitle(activity.getTitle());
            newActivity.setDescription(activity.getDescription());
            newActivity.setUserId(activity.getUserId());
            newActivity.setExperience(activity.getExperience());
            newActivity.setDifficulty(activity.getDifficulty());
            return newActivity;
        }
    }
}
