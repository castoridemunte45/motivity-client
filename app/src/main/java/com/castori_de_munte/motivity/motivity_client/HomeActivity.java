package com.castori_de_munte.motivity.motivity_client;

//import android.app.AlertDialog;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.castori_de_munte.motivity.motivity_client.fragments.Achievements;
import com.castori_de_munte.motivity.motivity_client.fragments.ActiveMissions;
import com.castori_de_munte.motivity.motivity_client.fragments.Activities;
import com.castori_de_munte.motivity.motivity_client.fragments.CreateActivity;
import com.castori_de_munte.motivity.motivity_client.fragments.CreateMission;
import com.castori_de_munte.motivity.motivity_client.fragments.Profile;
import com.castori_de_munte.motivity.motivity_client.fragments.ViewMission;
import com.castori_de_munte.motivity.motivity_client.services.AchievementsService;
import com.castori_de_munte.motivity.motivity_client.services.AuthService;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class HomeActivity extends AppCompatActivity {
    private DrawerLayout dlDrawer;
    private MenuItem previousItem;
    private FloatingActionButton fab;
    private NavigationView navigationView;
    private Bundle fragmentBundle;
    private List<String> fragmentsHistory = new ArrayList<>();
    private String menuItemFragment;
    private AlertDialog dialog;
    private ProgressDialog progress;


    private BroadcastReceiver receivedLogout = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            logout();
        }
    };


    private BroadcastReceiver receivedSync = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Bundle bundle = intent.getExtras();
            if(progress != null && progress.isShowing()) progress.dismiss();
            if(SharedFunctions.intentHasError(context.getApplicationContext(), bundle)){
                return;
            }
            handleSync();
        }
    };

    private BroadcastReceiver receivedChangeFragment = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Bundle bundle = intent.getExtras();
            fragmentBundle = new Bundle();

            if(SharedFunctions.intentHasError(context.getApplicationContext(), bundle)){
                return;
            }
            String fragmentType = bundle.getString("fragmentType");
            if (fragmentType == null) {
                if( Global.isDev) {
                    Snackbar.make(findViewById(R.id.coordinator_home), R.string.missing_fragment, Snackbar.LENGTH_SHORT).show();
                }
                return;
            }
            fragmentBundle = bundle;
            changeFragment(fragmentType);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        Snackbar.make(findViewById(R.id.coordinator_home), R.string.login_toast, Snackbar.LENGTH_SHORT).show();
        initToolbar();
        initDrawer();
        initFAB();
        getFragmentType();
        checkForAchievements();

        LocalBroadcastManager.getInstance(this).registerReceiver(receivedLogout,
                new IntentFilter("received-logout"));
        LocalBroadcastManager.getInstance(this).registerReceiver(receivedChangeFragment,
                new IntentFilter("change-fragment"));
        LocalBroadcastManager.getInstance(this).registerReceiver(receivedSync,
                new IntentFilter("received-sync"));
    }

    @Override
    public void onDestroy() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(receivedLogout);
        LocalBroadcastManager.getInstance(this).unregisterReceiver(receivedChangeFragment);
        LocalBroadcastManager.getInstance(this).unregisterReceiver(receivedSync);
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        if(dlDrawer.isDrawerOpen(GravityCompat.START)){
            dlDrawer.closeDrawers();
            return;
        }
        if(fragmentsHistory.size() > 1){
            String fragmentType = fragmentsHistory.get(fragmentsHistory.size() - 2);
            fragmentsHistory.remove(fragmentsHistory.size() - 1);
            changeFragment(fragmentType);
        }else{
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle(getResources().getString(R.string.logout_confirm));
            builder.setPositiveButton(getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    logout();
                }
            });

            builder.setNeutralButton(getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    // Do something when click the neutral button
                }
            });

            AlertDialog dialog = builder.create();
            dialog.show();
        }
    }

    private void getFragmentType(){
        Bundle bundle = getIntent().getExtras();
        String fragmentType = bundle.getString("fragmentType");
        if(fragmentType == null) return;

        changeFragment(fragmentType);
    }

    private void changeFragment(String fragmentType){
        if(fragmentsHistory.contains(fragmentType)){
            fragmentsHistory.remove(fragmentType);
        }
        fragmentsHistory.add(fragmentType);
        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
        fragmentTransaction.setCustomAnimations(
                R.animator.fragment_slide_left_enter,
                R.animator.fragment_slide_left_exit,
                R.animator.fragment_slide_right_enter,
                R.animator.fragment_slide_right_exit
        );
        if(fragmentType.equals("user-activities")){
            if(!fab.isShown()) fab.show();
            checkMenuItem(navigationView.getMenu().findItem(R.id.navigation_item_user_activities));
            Activities activities = new Activities();
            fragmentTransaction.replace(R.id.contentFrame, activities, fragmentType);
        }else
        if(fragmentType.equals("active-missions")){
            if(!fab.isShown()) fab.show();
            checkMenuItem(navigationView.getMenu().findItem(R.id.navigation_item_active_missions));
            ActiveMissions activeMissions = new ActiveMissions();
            fragmentTransaction.replace(R.id.contentFrame, activeMissions, fragmentType);
        }else
        if(fragmentType.equals("selected-mission")){
            fab.hide();
            ViewMission viewMission = new ViewMission();
            viewMission.setArguments(fragmentBundle);
            fragmentTransaction.replace(R.id.contentFrame, viewMission, fragmentType);
        }else
        /*if(fragmentType.equals("selected-friend")){
            fab.hide();
            ViewFriend viewFriend = new ViewFriend();
            viewFriend.setArguments(fragmentBundle);
            fragmentTransaction.replace(R.id.contentFrame, viewFriend, fragmentType);
        }else*/
        if(fragmentType.equals("profile")){
            fab.hide();
            checkMenuItem(navigationView.getMenu().findItem(R.id.navigation_item_profile));
            Profile profile = new Profile();
            fragmentTransaction.replace(R.id.contentFrame, profile, fragmentType);
        }else
        if(fragmentType.equals("create_activity")){
            fab.hide();
            CreateActivity createActivity = new CreateActivity();
            fragmentTransaction.replace(R.id.contentFrame, createActivity, fragmentType);
        }else
        if(fragmentType.equals("create-mission")){
            fab.hide();
            CreateMission createMission = new CreateMission();
            fragmentTransaction.replace(R.id.contentFrame, createMission, fragmentType);
        }else
        if(fragmentType.equals("user-achievements")){
            fab.hide();
            checkMenuItem(navigationView.getMenu().findItem(R.id.navigation_item_user_achievements));
            Achievements achievements = new Achievements();
            fragmentTransaction.replace(R.id.contentFrame, achievements, fragmentType);
        }else
        if(Global.isDev){
            Snackbar.make(findViewById(R.id.coordinator_home), R.string.missing_fragment, Snackbar.LENGTH_SHORT).show();
        }

        fragmentTransaction.commitAllowingStateLoss();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id) {
            case android.R.id.home:
                dlDrawer.openDrawer(GravityCompat.START);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void initFAB(){
        fab = (FloatingActionButton)findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(previousItem.getItemId() == R.id.navigation_item_user_activities)
                    changeFragment("create_activity");
                else
                if(previousItem.getItemId() == R.id.navigation_item_active_missions)
                    changeFragment("create-mission");
            }
        });
    }

    public void initToolbar(){
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setHomeAsUpIndicator(R.drawable.ic_menu);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    private void checkMenuItem(MenuItem menuItem){
        if(menuItem != null) {
            menuItem.setCheckable(true);
            menuItem.setChecked(true);
            if (previousItem != null) {
                previousItem.setChecked(false);
            }
        }else{
            navigationView.setCheckedItem(R.id.menu_none_checked);
        }
        previousItem = menuItem;
    }

    private void initDrawer(){
        dlDrawer = (DrawerLayout) findViewById(R.id.drawer_layout_home);

        navigationView = (NavigationView) findViewById(R.id.navigation_view);
//        Set the previousItem to be a default one and check it
        previousItem = navigationView.getMenu().findItem(R.id.navigation_item_active_missions);
        navigationView.getMenu().findItem(R.id.navigation_item_active_missions).setChecked(true);

//        If the user is not admin, then don't show it
//        if(!Global.user.getIsAdmin())
//            navigationView.getMenu().findItem(R.id.navigation_item_categories).setVisible(false);

        menuItemFragment = "active-missions";
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {
                dlDrawer.closeDrawers();
                if((menuItemFragment != null && menuItemFragment.equals(fragmentsHistory.get(fragmentsHistory.size() - 1))) &&
                        (previousItem != null && menuItem.equals(previousItem))) {
                    return true;
                }

                int id = menuItem.getItemId();
                switch (id) {
                    case R.id.action_logout:
                        AuthService authService = new AuthService(getApplicationContext(), "received-logout");
                        authService.logout();
                        return true;
                    case R.id.navigation_item_user_activities:
                        menuItemFragment = "user-activities";
                        changeFragment(menuItemFragment);
                        return true;
                    case R.id.navigation_item_active_missions:
                        menuItemFragment = "active-missions";
                        changeFragment(menuItemFragment);
                        return true;
                    case R.id.navigation_item_user_achievements:
                        menuItemFragment = "user-achievements";
                        changeFragment(menuItemFragment);
                        return true;
                    case R.id.navigation_item_profile:
                        menuItemFragment = "profile";
                        changeFragment(menuItemFragment);
                        return true;
                    case R.id.navigation_item_sync:
                        askForSync();
                        return true;
//                    case R.id.navigation_item_categories:
//                        changeFragment("categories");
//                        return true;
                }
                return true;
            }
        });

        TextView drawerUser = (TextView) navigationView.getHeaderView(0).findViewById(R.id.drawer_header_user);
        String displayedUser = Global.user.getFirstName() + " " + Global.user.getLastName() + ": " + Global.user.getEmail();
        drawerUser.setText(displayedUser);
    }

    private void askForSync(){
        AlertDialog.Builder builder = new AlertDialog.Builder(HomeActivity.this);
        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View layout = inflater.inflate(R.layout.dialog_sync_account,
                (ViewGroup) findViewById(R.id.root_element));

        builder.setTitle(getResources().getString(R.string.sync_account_short));
        final EditText etEmail = (EditText) layout.findViewById(R.id.etEmail);
        final EditText etPassword = (EditText) layout.findViewById(R.id.etPassword);

        if(Global.isDev){
            etEmail.setText("dumpaul33@gmail.com");
            etPassword.setText("12345");
        }

        builder.setPositiveButton(R.string.ok, null);
        builder.setNegativeButton(R.string.cancel, null);

        builder.setIcon(android.R.drawable.ic_popup_sync);
        builder.setView(layout);
        dialog = builder.create();
        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                Button button = ((AlertDialog) dialog).getButton(AlertDialog.BUTTON_POSITIVE);
                button.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        progress = new ProgressDialog(HomeActivity.this);
                        progress.setTitle(getResources().getString(R.string.action_sync_text));
                        progress.setMessage(getResources().getString(R.string.please_wait));
                        progress.show();
                        AuthService authService = new AuthService(getApplicationContext(), "received-sync", Global.REMOTE_API_LOCATION);
                        authService.sync(etEmail.getText().toString(), etPassword.getText().toString());
                    }
                });
            }
        });
        dialog.show();
    }

    private void handleSync(){
        SweetAlertDialog sd =  new SweetAlertDialog(this);
        sd.setTitleText(getString(R.string.sync_completed));
        sd.setCancelable(true);
        sd.setCanceledOnTouchOutside(true);
        sd.show();
        dialog.dismiss();
    }

    private void checkForAchievements(){
        final AchievementsService achievementsService = new AchievementsService(HomeActivity.this, "check-achievements");
        achievementsService.checkAchievements();
        final Handler ha=new Handler();
        ha.postDelayed(new Runnable() {
            @Override
            public void run() {
                achievementsService.checkAchievements();
                ha.postDelayed(this, 10000);
            }
        }, 10000);
    }

    private void logout(){
        SharedPreferences shared = getSharedPreferences("shared", MODE_PRIVATE);
        SharedPreferences.Editor editor = shared.edit();
        editor.remove("authToken");
        editor.apply();
        Global.clear();

        finishAffinity();
//        Intent loginActivity = new Intent(this, LoginActivity.class);
//        loginActivity.putExtra("activity", "home");
//        startActivity(loginActivity);
    }
}
