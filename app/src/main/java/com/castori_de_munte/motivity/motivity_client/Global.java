package com.castori_de_munte.motivity.motivity_client;

import android.content.Context;

import com.castori_de_munte.motivity.motivity_client.models.ActivityObj;
import com.castori_de_munte.motivity.motivity_client.models.FriendObj;
import com.castori_de_munte.motivity.motivity_client.models.MissionObj;
import com.castori_de_munte.motivity.motivity_client.models.UserAchievementObj;
import com.castori_de_munte.motivity.motivity_client.models.UserObj;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class Global {
    public static String API_LOCATION = "http://ask-around.ngenh.com:6542/api/";
    public static String REMOTE_API_LOCATION = "http://cristi.red:8080/api/";

    public static boolean isDev = true;
    public static int connectionTimeout = 5000;
    public static int maxActivities = 5;

    public static UserObj.User user;
    public static String authToken;

    public final static List<String> difficulties;
    public final static Map<String, String> defaultOptions;

    static {
        difficulties = new ArrayList<>();
        difficulties.add("Not even trying");
        difficulties.add("Trivial");
        difficulties.add("Just starting");
        difficulties.add("Might pose a challenge");
        difficulties.add("Bring it on");
        difficulties.add("You still got this");
        difficulties.add("Can give headaches");
        difficulties.add("Difficult");
        difficulties.add("Insane");
        difficulties.add("Impossible");

        defaultOptions = new HashMap<>();
        defaultOptions.put("page", "0");
        defaultOptions.put("pageSize", "10");

    }

    public static List<UserAchievementObj.UserAchievement> userAchievements;
    public static UserAchievementObj.UserAchievement userAchievement;
    public static int userAchievementPosition;
    public static Map<String, String> userAchievementsOptions;

    public static List<MissionObj.Mission> missions;
    public static MissionObj.Mission mission;
    public static int missionPosition;
    public static Map<String, String> missionsOptions;

    public static List<ActivityObj.Activity> activities;
    public static int activityPosition;
    public static Map<String, String> activitiesOptions;

    public static List<FriendObj.Friend> friends;
    public static FriendObj.Friend friend;
    public static int friendPosition;
    public static Map<String, String> friendsOptions;

    public static void clear(){
        user = null;
        authToken = null;
    }

    public static void initialize(Context context){
        if(user == null) user = new UserObj.User();
        missions = new ArrayList<>();
        activities = new ArrayList<>();
        friends=new ArrayList<>();
        userAchievements = new ArrayList<>();
        missionsOptions = new HashMap<>(defaultOptions);
        activitiesOptions = new HashMap<>(defaultOptions);
        userAchievementsOptions = new HashMap<>(defaultOptions);
        friendsOptions=new HashMap<>(defaultOptions);
    }

}
