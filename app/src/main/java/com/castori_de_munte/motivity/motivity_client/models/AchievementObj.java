package com.castori_de_munte.motivity.motivity_client.models;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class AchievementObj {
    @JsonCreator
    public AchievementObj(){
        this.achievement = new Achievement();
    }

    @JsonProperty("achievement")
    private Achievement achievement;

    @JsonProperty("achievement")
    public Achievement getAchievement() {
        return achievement;
    }

    @JsonProperty("achievement")
    public void setAchievement(Achievement achievement) {
        this.achievement = achievement;
    }

    public static class Achievement{
        @JsonCreator
        public Achievement() {
            this.title = "";
            this.description = "";
            this.milestone=0;

        }

        @JsonProperty("id")
        private int id;

        @JsonProperty("title")
        private String title;

        @JsonProperty("description")
        private String description;

        @JsonProperty("milestone")
        private int milestone;

        @JsonProperty("target")
        private String target;

        @JsonProperty("id")
        public int getId() {
            return id;
        }

        @JsonProperty("id")
        public void setId(int id) {
            this.id = id;
        }

        @JsonProperty("title")
        public String getTitle() {
            return title;
        }

        @JsonProperty("title")
        public void setTitle(String title) {
            this.title = title;
        }

        @JsonProperty("description")
        public String getDescription() {
            return description;
        }

        @JsonProperty("description")
        public void setDescription(String description) {
            this.description = description;
        }

        @JsonProperty("milestone")
        public int getMilestone() {
            return milestone;
        }

        @JsonProperty("milestone")
        public void setMilestone(int milestone) {
            this.milestone = milestone;
        }

        @JsonProperty("target")
        public String getTarget() {
            return target;
        }

        @JsonProperty("target")
        public void setTarget(String target) {
            this.target = target;
        }
    }

}
