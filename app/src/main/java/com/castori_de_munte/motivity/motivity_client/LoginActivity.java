package com.castori_de_munte.motivity.motivity_client;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.support.design.widget.Snackbar;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatCheckBox;
import android.view.View;
import android.widget.EditText;

import com.castori_de_munte.motivity.motivity_client.custom_controls.NetworkStatus;
import com.castori_de_munte.motivity.motivity_client.services.AuthService;
import com.castori_de_munte.motivity.motivity_client.services.UsersService;

public class LoginActivity extends AppCompatActivity {

    private ProgressDialog progress;
    private EditText etEmail;
    private EditText etPassword;
    private AppCompatCheckBox cbRememberCredentials;
    private Snackbar snackbar;

    private BroadcastReceiver receivedLogin = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Bundle bundle = intent.getExtras();
            if(progress != null && progress.isShowing()) progress.dismiss();
            if(SharedFunctions.intentHasError(context.getApplicationContext(), bundle)){
                return;
            }
            handleLogin();
        }
    };

    private BroadcastReceiver receivedUser = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Bundle bundle = intent.getExtras();
            if(progress != null && progress.isShowing()) progress.dismiss();
            if(bundle != null) {
                String error = bundle.getString("error");
                if (error != null) {
                    SharedPreferences shared = getSharedPreferences("shared", MODE_PRIVATE);
                    SharedPreferences.Editor editor = shared.edit();
                    editor.remove("authToken");
                    editor.apply();
                    snackbar = Snackbar.make(findViewById(R.id.drawer_layout_login), error, Snackbar.LENGTH_SHORT);
                    snackbar.show();
                    return;
                }
            }
            handleLogin();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        LocalBroadcastManager.getInstance(this).registerReceiver(receivedLogin,
                new IntentFilter("received-login"));
        LocalBroadcastManager.getInstance(this).registerReceiver(receivedUser,
                new IntentFilter("received-user"));

        initUI();
        initIntent();
        if(NetworkStatus.getInstance(getApplicationContext()).isNetworkAvailable())
            getToken();
    }

    @Override
    public void onDestroy() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(receivedLogin);
        LocalBroadcastManager.getInstance(this).unregisterReceiver(receivedUser);
        super.onDestroy();
    }

    private void initIntent(){
        Bundle extras = getIntent().getExtras();
        if(extras != null){
            String email = extras.getString("email");
            String password = extras.getString("password");
            if(email != null && password != null) {
                etEmail.setText(email);
                etPassword.setText(password);
                login(null);
            }
        }
    }

    private void initUI(){
        etEmail = (EditText) findViewById(R.id.etEmail);
        etPassword = (EditText) findViewById(R.id.etPassword);
        cbRememberCredentials = (AppCompatCheckBox) findViewById(R.id.cbRememberCredentials);

        if(Global.isDev) {
            etEmail.setText("admin@motivity.com");
            etPassword.setText("123456");
        }
    }

    private void getToken(){
        SharedPreferences shared = getSharedPreferences("shared", MODE_PRIVATE);
        if(shared.contains("authToken")){
            Global.authToken = shared.getString("authToken", null);
            if(Global.authToken != null){
                progress = new ProgressDialog(this);
                progress.setTitle(getResources().getString(R.string.action_login_text));
                progress.setMessage(getResources().getString(R.string.please_wait));
                progress.show();
                UsersService usersService = new UsersService(getApplicationContext(), "received-user");
                usersService.getMe();
            }
        }
    }

    public void login(final View view) {
        boolean ok = true;

        if(! android.util.Patterns.EMAIL_ADDRESS.matcher(etEmail.getText().toString()).matches()) {
            etEmail.setError(getResources().getString(R.string.invalid_email));
            snackbar = Snackbar.make(findViewById(R.id.drawer_layout_login), R.string.invalid_email, Snackbar.LENGTH_SHORT);
            snackbar.show();
            ok = false;
        }
        if(etEmail.getText().toString().isEmpty() || etPassword.getText().toString().isEmpty()){
            if(etEmail.getText().toString().isEmpty()) etEmail.setError(getResources().getString(R.string.field_required));
            if(etPassword.getText().toString().isEmpty()) etPassword.setError(getResources().getString(R.string.field_required));
            ok = false;
            snackbar = Snackbar.make(findViewById(R.id.drawer_layout_login), R.string.fields_empty, Snackbar.LENGTH_SHORT);
            snackbar.show();
        }

        if (!ok)return;

        progress = new ProgressDialog(this);
        progress.setTitle(getResources().getString(R.string.action_login_text));
        progress.setMessage(getResources().getString(R.string.please_wait));
        progress.show();

        AuthService authService = new AuthService(getApplicationContext(), "received-login");
        authService.login(etEmail.getText().toString(), etPassword.getText().toString());
    }

    public void saveInformation() {
        SharedPreferences shared = getSharedPreferences("shared", MODE_PRIVATE);
        SharedPreferences.Editor editor = shared.edit();
        editor.putString("authToken", Global.authToken);
        editor.apply();
    }

    private void handleLogin(){
        if(cbRememberCredentials != null && cbRememberCredentials.isChecked()) saveInformation();
        Global.initialize(getApplicationContext());
        if(!Global.user.getSeenTutorial()){
            Intent tutorialActivity = new Intent(LoginActivity.this, TutorialActivity.class);
            startActivity(tutorialActivity);
        } else {
            Intent homeActivity = new Intent(LoginActivity.this, HomeActivity.class);
            homeActivity.putExtra("fragmentType", "active-missions");
            startActivity(homeActivity);
        }
    }


    public void goToRegister(View view) {
        Intent i = new Intent(LoginActivity.this, RegisterActivity.class);
        startActivity(i);
    }
}
