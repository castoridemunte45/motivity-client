package com.castori_de_munte.motivity.motivity_client.services;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;

import com.castori_de_munte.motivity.motivity_client.Global;
import com.castori_de_munte.motivity.motivity_client.R;
import com.castori_de_munte.motivity.motivity_client.SharedFunctions;
import com.castori_de_munte.motivity.motivity_client.middleware.ServiceCreator;
import com.castori_de_munte.motivity.motivity_client.models.ActivityObj;
import com.castori_de_munte.motivity.motivity_client.models.Auth;
import com.castori_de_munte.motivity.motivity_client.models.SyncActivity;
import com.castori_de_munte.motivity.motivity_client.models.UserObj;

import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AuthService {
    public HashMap<String, Boolean> actions;
    private Context context;
    private com.castori_de_munte.motivity.motivity_client.interfaces.AuthService authService;
    private String eventName;


    public AuthService(Context context, String eventName){
        this.actions = new HashMap<>();
        this.context = context;
        this.eventName = eventName;
        this.authService =
                ServiceCreator.retrofitService(com.castori_de_munte.motivity.motivity_client.interfaces.AuthService.class, context);
    }

    public AuthService(Context context, String eventName, String BASE_URL){
        this.actions = new HashMap<>();
        this.context = context;
        this.eventName = eventName;
        this.authService =
                ServiceCreator.retrofitService(com.castori_de_munte.motivity.motivity_client.interfaces.AuthService.class, context, BASE_URL);
    }

    public void login(String email, String password){
        if ((actions.containsKey("login") && actions.get("login")) || SharedFunctions.checkNetworkStatus(context, authService, eventName)) return;
        else
            actions.put("login", true);
        Auth authData = new Auth();

        authData.getCredentials().setEmail(email);
        authData.getCredentials().setPassword(password);

        Call<UserObj> call = authService.login(authData);
        call.enqueue(new Callback<UserObj>() {
            @Override
            public void onResponse(Call<UserObj> call, Response<UserObj> response) {
                int statusCode = response.code();
                actions.remove("login");
                Intent intent = new Intent(eventName);

                if (statusCode == 200) {
                    Global.user = response.body().getUser();
                    Global.authToken = response.headers().get("Set-Cookie").split(";")[0];
                } else
                if (statusCode == 403) {
                    intent.putExtra("error", context.getResources().getString(R.string.credentials_error));
                } else {
                    String message = Global.isDev ?
                            response.message() : context.getResources().getString(R.string.error_generic);
                    intent.putExtra("error", message);
                }
                LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
            }

            @Override
            public void onFailure(Call<UserObj> call, Throwable t) {
                SharedFunctions.failing(context, t, eventName);
            }
        });
    }

    public void sync(String email, String password){
        if ((actions.containsKey("sync") && actions.get("sync")) || SharedFunctions.checkNetworkStatus(context, authService, eventName)) return;
        else
            actions.put("sync", true);
        Auth authData = new Auth();

        authData.getCredentials().setEmail(email);
        authData.getCredentials().setPassword(password);

        Call<List<SyncActivity>> call = authService.sync(authData.getCredentials());
        call.enqueue(new Callback<List<SyncActivity>>() {
            @Override
            public void onResponse(Call<List<SyncActivity>> call, Response<List<SyncActivity>> response) {
                int statusCode = response.code();
                actions.remove("sync");
                Intent intent = new Intent(eventName);

                if (statusCode == 200) {
                    // Authentication Ok. Save data in database.
                    ActivitiesService activitiesService = new ActivitiesService(context, "received-service");
                    for(int i = 0; i < response.body().size(); i++){
                        ActivityObj activityObj = new ActivityObj();
                        activityObj.getActivity().setTitle(response.body().get(i).getName());
                        activityObj.getActivity().setDescription(response.body().get(i).getDescription());
                        activityObj.getActivity().setDifficulty(response.body().get(i).getDifficulty());
                        activityObj.getActivity().setUserId(Global.user.getId());
                        activitiesService.insert(activityObj, false);
                    }
                } else
                if (statusCode == 403) {
                    intent.putExtra("error", context.getResources().getString(R.string.credentials_error));
                } else {
                    String message = Global.isDev ?
                            response.message() : context.getResources().getString(R.string.error_generic);
                    intent.putExtra("error", message);
                }
                LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
            }

            @Override
            public void onFailure(Call<List<SyncActivity>> call, Throwable t) {
                SharedFunctions.failing(context, t, eventName);
            }
        });
    }


    public void logout() {
        if ((actions.containsKey("logout") && actions.get("logout")) || SharedFunctions.checkNetworkStatus(context, authService, eventName)) return;
        else
            actions.put("logout", true);
        Call<String> call = authService.logout();
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                int statusCode = response.code();
                actions.remove("logout");
                Intent intent = new Intent(eventName);

                if (statusCode == 200) {
                    Global.authToken = null;
                }else {
                    String message = Global.isDev ?
                            response.message() : context.getResources().getString(R.string.error_generic);
                    intent.putExtra("error", message);
                }
                LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                SharedFunctions.failing(context, t, eventName);
            }
        });
    }

    public void register(String firstName, String lastName, String email, String password){
        if ((actions.containsKey("register") && actions.get("register")) || SharedFunctions.checkNetworkStatus(context, authService, eventName)) return;
        else
            actions.put("register", true);
        UserObj userObj = new UserObj();

        userObj.getUser().setFirstName(firstName);
        userObj.getUser().setLastName(lastName);
        userObj.getUser().setEmail(email);
        userObj.getUser().setPassword(password);

        Call<UserObj> call = authService.register(userObj);
        call.enqueue(new Callback<UserObj>() {
            @Override
            public void onResponse(Call<UserObj> call, Response<UserObj> response) {
                int statusCode = response.code();
                Intent intent = new Intent(eventName);

                if (statusCode == 200) {

                } else {
                    String message = Global.isDev ?
                            response.message() : context.getResources().getString(R.string.error_generic);
                    intent.putExtra("error", message);
                }
                LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
            }

            @Override
            public void onFailure(Call<UserObj> call, Throwable t) {
                SharedFunctions.failing(context, t, eventName);
            }
        });
    }

    public void checkAvailability(){
        if ((actions.containsKey("check") && actions.get("check")) || SharedFunctions.checkNetworkStatus(context, authService, eventName)) return;
        else
            actions.put("check", true);
        Call<String> call = authService.checkAvailability();
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                int statusCode = response.code();
                actions.remove("check");
                Intent intent = new Intent(eventName);

                if (statusCode == 200) {

                }else {
                    String message = Global.isDev ?
                            response.message() : context.getResources().getString(R.string.error_generic);
                    intent.putExtra("error", message);
                }
                LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                SharedFunctions.failing(context, t, eventName);
            }
        });
    }
}
