package com.castori_de_munte.motivity.motivity_client.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import java.util.List;

import com.castori_de_munte.motivity.motivity_client.Global;
import com.castori_de_munte.motivity.motivity_client.R;
import com.castori_de_munte.motivity.motivity_client.SharedFunctions;
import com.castori_de_munte.motivity.motivity_client.models.UserAchievementObj;

public class UserAchievementsAdapter extends RecyclerView.Adapter<UserAchievementsAdapter.ViewHolder>{
    private LayoutInflater inflater;
    private List<UserAchievementObj.UserAchievement> userAchievements;
    private Context context;

    public UserAchievementsAdapter(Context context, List<UserAchievementObj.UserAchievement> userAchievements){
        inflater = LayoutInflater.from(context);
        this.context = context;
        this.userAchievements = userAchievements;
    }

    @Override
    public UserAchievementsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.fragment_user_achievement_view_holder, parent, false);
        UserAchievementsAdapter.ViewHolder holder = new UserAchievementsAdapter.ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(UserAchievementsAdapter.ViewHolder holder, int position) {
        UserAchievementObj.UserAchievement userAchievement = userAchievements.get(position);
        holder.tvTitle.setText(userAchievement.getAchievement().getTitle());
        holder.tvDescription.setText(userAchievement.getAchievement().getDescription());
        holder.id = userAchievement.getId();

        holder.milestone = userAchievement.getAchievement().getMilestone();
        holder.target = userAchievement.getAchievement().getTarget();
        String stMilestoneAndTarget = "Need: " + holder.milestone + " x " + holder.target;
        holder.tvMilestoneAndTarget.setText(stMilestoneAndTarget);

        if(!userAchievement.getIsCompleted()) {
            holder.progress = userAchievement.getProgress();
            holder.pbAchievementProgress.getProgressDrawable().setColorFilter(
                    ContextCompat.getColor(context,R.color.red), android.graphics.PorterDuff.Mode.SRC_IN);
            holder.percentage = (float)(holder.progress * 100.0f /
                    (float)(holder.milestone));
            holder.percentage = SharedFunctions.roundFloat(holder.percentage, 2);

            SharedFunctions.setMax(holder.pbAchievementProgress, holder.milestone, 100);
            SharedFunctions.setProgress(holder.pbAchievementProgress, holder.progress, 100);
            String progressText = "In progress: " + holder.percentage + " %";
            holder.tvAchievementProgress.setText(progressText);
            holder.tvDateAcquired.setVisibility(View.GONE);
            holder.pbAchievementProgress.setVisibility(View.VISIBLE);
        }else{
            holder.tvAchievementProgress.setText(context.getResources().getString(R.string.completed));
            String stDateAcquired = "Ended at:" + UserAchievementObj.UserAchievement.getCreatedAtAsString(userAchievement);
            holder.tvDateAcquired.setText(stDateAcquired);
            holder.tvDateAcquired.setVisibility(View.VISIBLE);
            holder.pbAchievementProgress.setVisibility(View.GONE);

        }
    }

    @Override
    public int getItemCount() {
        return userAchievements.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView tvTitle;
        private TextView tvDescription;
        private TextView tvAchievementProgress;
        private TextView tvMilestoneAndTarget;
        private TextView tvDateAcquired;
        private ProgressBar pbAchievementProgress;
        private int id;
        private float percentage;
        private int progress;
        private int milestone;
        private String target;

        public ViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            tvTitle = (TextView) itemView.findViewById(R.id.tvTitle);
            tvDescription = (TextView) itemView.findViewById(R.id.tvDescription);
            tvAchievementProgress = (TextView) itemView.findViewById(R.id.tvProgress);
            pbAchievementProgress = (ProgressBar) itemView.findViewById(R.id.pbProgress);
            tvMilestoneAndTarget = (TextView) itemView.findViewById(R.id.tvMilestoneAndTarget);
            tvDateAcquired = (TextView) itemView.findViewById(R.id.tvDateAcquired);
        }

        @Override
        public void onClick(View view) {
            Intent intent = new Intent("selected-achievement");
            intent.putExtra("achievementId", id);
            intent.putExtra("title", tvTitle.getText().toString());
            intent.putExtra("description", tvDescription.getText().toString());
            intent.putExtra("percentage", percentage);
            intent.putExtra("progress", progress);
            intent.putExtra("milestone", milestone);
            intent.putExtra("target", target);
            intent.putExtra("achievementProgress", tvAchievementProgress.getText().toString());
            intent.putExtra("milestone", tvMilestoneAndTarget.getText().toString());
            intent.putExtra("dateAcquired", tvDateAcquired.getText().toString());

            for(int i = 0; i < userAchievements.size(); i++){
                if(userAchievements.get(i).getId() == id){
                    Global.userAchievement = UserAchievementObj.UserAchievement.copyUserAchievement(userAchievements.get(i));
                    Global.userAchievementPosition = i;
                    break;
                }
            }

            LocalBroadcastManager.getInstance(context).sendBroadcast(intent);

        }
    }
}
