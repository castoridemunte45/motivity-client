package com.castori_de_munte.motivity.motivity_client.middleware;

import android.content.Context;

import com.castori_de_munte.motivity.motivity_client.Global;
import com.castori_de_munte.motivity.motivity_client.custom_controls.NetworkStatus;

import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;

public class ServiceCreator {

    public static <T> T retrofitService(final Class<T> requiredClass, Context context) {
        if(!NetworkStatus.getInstance(context).isNetworkAvailable()){
            return null;
        }
        Retrofit.Builder retrofitBuilder = new Retrofit.Builder()
                .baseUrl(Global.API_LOCATION)
                .addConverterFactory(JacksonConverterFactory.create());

        if (Global.authToken != null) {
            retrofitBuilder.client(HttpInterceptor.authInterceptor());
        }

        return retrofitBuilder.build().create(requiredClass);
    }

    public static <T> T retrofitService(final Class<T> requiredClass, Context context, String BASE_URL) {
        if(!NetworkStatus.getInstance(context).isNetworkAvailable()){
            return null;
        }
        Retrofit.Builder retrofitBuilder = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(JacksonConverterFactory.create());

//        if (Global.authToken != null) {
//            retrofitBuilder.client(HttpInterceptor.authInterceptor());
//        }

        return retrofitBuilder.build().create(requiredClass);
    }
}
