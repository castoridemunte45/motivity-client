package com.castori_de_munte.motivity.motivity_client.models;

import com.castori_de_munte.motivity.motivity_client.custom_controls.ISO8601;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;


@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class UserAchievementObj {
    @JsonCreator
    public UserAchievementObj() {
        this.userAchievement = new UserAchievement();
    }

    @JsonProperty("user_achievement")
    private UserAchievement userAchievement;

    @JsonProperty("user_achievement")
    public UserAchievement getUserAchievement() {
        return userAchievement;
    }

    @JsonProperty("user_achievement")
    public void setUserAchievement(UserAchievement userAchievements) {
        this.userAchievement = userAchievements;
    }

    public static class UserAchievement{
        @JsonCreator
        public UserAchievement() {
            this.completed=false;
            this.progress=0;
        }

        @JsonProperty("id")
        private int id;

        @JsonProperty("user_id")
        private int userId;

        @JsonProperty("achievement_id")
        private int achievementId;

        @JsonProperty("progress")
        private int progress;

        @JsonProperty("completed")
        private boolean completed;

        @JsonProperty("date_acquired")
        private String dateAcquired;

        @JsonProperty("user")
        private UserObj.User user;

        @JsonProperty("achievement")
        private AchievementObj.Achievement achievement;

        @JsonProperty("id")
        public int getId() {
            return id;
        }

        @JsonProperty("id")
        public void setId(int id) {
            this.id = id;
        }

        @JsonProperty("user_id")
        public int getUserId() {
            return userId;
        }

        @JsonProperty("user_id")
        public void setUserId(int userId) {
            this.userId = userId;
        }

        @JsonProperty("achievement_id")
        public int getAchievementId() {
            return achievementId;
        }

        @JsonProperty("achievement_id")
        public void setAchievementId(int achievementId) {
            this.achievementId = achievementId;
        }
        @JsonProperty("progress")
        public int getProgress() {
            return progress;
        }

        @JsonProperty("progress")
        public void setProgress(int progress) {
            this.progress = progress;
        }

        @JsonProperty("completed")
        public boolean getIsCompleted() {
            return completed;
        }

        @JsonProperty("completed")
        public void setIsCompleted(boolean completed) {
            this.completed = completed;
        }

        @JsonProperty("date_acquired")
        public String getDateAcquired() {
            return dateAcquired;
        }

        @JsonProperty("date_acquired")
        public void setDateAcquired(String dateAcquired) {
            this.dateAcquired = dateAcquired;
        }

        @JsonProperty("achievement")
        public AchievementObj.Achievement getAchievement() {
            return achievement;
        }

        @JsonProperty("achievement")
        public void setAchievement(AchievementObj.Achievement achievement) {
            this.achievement = achievement;
        }

        @JsonProperty("user")
        public UserObj.User getUser() {
            return user;
        }

        @JsonProperty("user")
        public void setUser(UserObj.User user) {
            this.user = user;
        }

        public static UserAchievementObj.UserAchievement copyUserAchievement(UserAchievementObj.UserAchievement userAchievement){
            UserAchievement newUserAchievement = new UserAchievement();
            newUserAchievement.setId(userAchievement.getId());
            newUserAchievement.setUserId(userAchievement.getUserId());
            newUserAchievement.setAchievementId(userAchievement.getAchievementId());
            newUserAchievement.setUserId(userAchievement.getUserId());
            newUserAchievement.setIsCompleted(userAchievement.getIsCompleted());
            newUserAchievement.setProgress(userAchievement.getProgress());
            newUserAchievement.setAchievement(userAchievement.getAchievement());
            newUserAchievement.setProgress(userAchievement.getProgress());
            newUserAchievement.setDateAcquired(userAchievement.getDateAcquired());
            newUserAchievement.setUser(userAchievement.getUser());
            return newUserAchievement;
        }

        public static String getCreatedAtAsString(UserAchievementObj.UserAchievement userAchievement){
            if(userAchievement.getDateAcquired() == null || userAchievement.dateAcquired.equals("")) return "";
            Calendar date = ISO8601.toCalendar(userAchievement.getDateAcquired());
            SimpleDateFormat sdf = new SimpleDateFormat("EEE MMM dd HH:mm:ss zzz yyyy", Locale.ROOT);
            if(date == null) return null;
            Date d = date.getTime();
            String aux[] = sdf.format(d).split(" ");
            return aux[3] + " " + aux[2] + "/" + aux[1] + "/" + aux[5] + "  ";
        }
    }
}

