package com.castori_de_munte.motivity.motivity_client.adapters;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.castori_de_munte.motivity.motivity_client.Global;
import com.castori_de_munte.motivity.motivity_client.R;
import com.castori_de_munte.motivity.motivity_client.SharedFunctions;
import com.castori_de_munte.motivity.motivity_client.custom_controls.ISO8601;
import com.castori_de_munte.motivity.motivity_client.models.ActivityObj;
import com.castori_de_munte.motivity.motivity_client.models.MissionObj;
import com.castori_de_munte.motivity.motivity_client.services.MissionsService;

import java.sql.Date;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

public class MissionsAdapter extends RecyclerView.Adapter<MissionsAdapter.ViewHolder> {
    private LayoutInflater inflater;
    private List<MissionObj.Mission> missions;
    private Context context;
    private MissionsService missionsService;

    public MissionsAdapter(Context context, List<MissionObj.Mission> missions){
        inflater = LayoutInflater.from(context);
        this.context = context;
        this.missions = missions;
        missionsService =  new MissionsService(context.getApplicationContext(),"delete-mission");
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.fragment_mission_view_holder, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(MissionsAdapter.ViewHolder holder, int position) {
        MissionObj.Mission mission = missions.get(position);
        holder.tvTitle.setText(mission.getTitle());
        holder.tvDescription.setText(mission.getDescription());

        holder.pbMissionProgress.getProgressDrawable().setColorFilter(
                ContextCompat.getColor(context,R.color.red), android.graphics.PorterDuff.Mode.SRC_IN);
        holder.percentage = (float)(mission.getProgress()) * 100.0f /
                (float)(mission.getMilestone());
        holder.percentage = SharedFunctions.roundFloat(holder.percentage, 2);

        String expiredTime =  ISO8601.fromCalendar(ISO8601.dateAddHours(mission.getCreatedAt(),mission.getDuration()));
        float timePassed = ISO8601.timeBetweenDatesAsMilliseconds(ISO8601.now(),expiredTime);
        if(timePassed<=0 && !mission.getIsCompleted())
            mission.setExpired(true);

        String progressText = "In progress: " + holder.percentage + " %";
        if(mission.getIsCompleted())
        {
            progressText = context.getResources().getString(R.string.completed);
        }else if(mission.getIsExpired())
        {
            progressText=context.getResources().getString(R.string.expired);
        }
        holder.tvMissionProgress.setText(progressText);
        SharedFunctions.setMax(holder.pbMissionProgress, mission.getMilestone(), 100);
        SharedFunctions.setProgress(holder.pbMissionProgress, mission.getProgress(), 100);

        holder.id = mission.getId();
        holder.progress = mission.getProgress();
        holder.milestone = mission.getMilestone();

        if(mission.getIsExpired())
        {
            holder.pbProgressTime.setVisibility(View.GONE);
            holder.tvTimeLeft.setVisibility(View.GONE);
        }else if(!mission.getIsCompleted()) {
            holder.createdAtText = "Started:" + MissionObj.Mission.getCreatedAtAsString(mission);

            holder.expiresAtText = "Expires:" + ISO8601.formatDate(ISO8601.dateAddHours(mission.getCreatedAt(), mission.getDuration()));
            holder.pbProgressTime.getProgressDrawable().setColorFilter(
                    ContextCompat.getColor(context, R.color.blue), android.graphics.PorterDuff.Mode.SRC_IN);
            float progress = ISO8601.timeBetweenDatesAsMilliseconds(mission.getCreatedAt(), ISO8601.now());
            float max = ISO8601.timeBetweenDatesAsMilliseconds(
                    ISO8601.dateInMilliseconds(mission.getCreatedAt()),
                    ISO8601.dateInMilliseconds(ISO8601.dateAddHours(mission.getCreatedAt(), mission.getDuration()))
            );
            holder.tvTimeLeft.setText(ISO8601.friendlyTime((long) progress, (long) max));

            progress *= 0.0001f;
            max *= 0.0001f;
            holder.timeProgress = (long) progress;
            holder.timeMax = (long) max;
            SharedFunctions.setMax(holder.pbProgressTime, (int) max, 1f);
            SharedFunctions.setProgress(holder.pbProgressTime, (int) progress, 1f);
            holder.pbProgressTime.setVisibility(View.VISIBLE);
            holder.tvTimeLeft.setVisibility(View.VISIBLE);
        }
        else{
            holder.pbProgressTime.setVisibility(View.GONE);
            holder.tvTimeLeft.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return missions.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {
        private TextView tvTitle;
        private TextView tvDescription;
        private TextView tvMissionProgress;
        private TextView tvTimeLeft;
        private ProgressBar pbProgressTime;
        private ProgressBar pbMissionProgress;
        private int id;
        private float percentage;
        private int milestone;
        private int progress;
        private long timeProgress;
        private long timeMax;
        private String createdAtText;
        private String expiresAtText;

        public ViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            itemView.setOnLongClickListener(this);
            tvTitle = (TextView) itemView.findViewById(R.id.tvTitle);
            tvDescription = (TextView) itemView.findViewById(R.id.tvDescription);
            tvTimeLeft = (TextView) itemView.findViewById(R.id.tvTimeLeft);
            tvMissionProgress = (TextView) itemView.findViewById(R.id.tvProgress);
            pbProgressTime = (ProgressBar) itemView.findViewById(R.id.pbProgressTime);
            pbMissionProgress = (ProgressBar) itemView.findViewById(R.id.pbProgress);
        }

        @Override
        public void onClick(View view) {
            Intent intent = new Intent("selected-mission");
            intent.putExtra("missionId", id);
            intent.putExtra("title", tvTitle.getText().toString());
            intent.putExtra("description", tvDescription.getText().toString());
            intent.putExtra("percentage", percentage);
            intent.putExtra("progress", progress);
            intent.putExtra("progressText", tvMissionProgress.getText().toString());
            intent.putExtra("milestone", milestone);
            intent.putExtra("timeProgress", timeProgress);
            intent.putExtra("timeMax", timeMax);
            intent.putExtra("createdAtText", createdAtText);
            intent.putExtra("expiresAtText", expiresAtText);

            for(int i = 0; i < missions.size(); i++){
                if(missions.get(i).getId() == id){
                    Global.mission = MissionObj.Mission.copyMission(missions.get(i));
                    Global.missionPosition = i;
                    break;
                }
            }
            LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
        }

        @Override
        public boolean onLongClick(View view) {
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setTitle("Delete mission");
            builder.setMessage("Are you sure you want to delete this mission?");
            builder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    missionsService.delete(id);

                }
            });
            builder.setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                }
            });
            builder.setIcon(android.R.drawable.ic_dialog_alert);
            AlertDialog dialog = builder.create();
            dialog.show();

            return true;
        }
    }
}
