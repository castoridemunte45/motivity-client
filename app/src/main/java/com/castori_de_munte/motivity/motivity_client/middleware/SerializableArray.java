package com.castori_de_munte.motivity.motivity_client.middleware;

import java.io.Serializable;
import java.util.List;

public class SerializableArray<T> implements Serializable {
    private List<T> array;

    public SerializableArray(List<T> data) {
        this.array = data;
    }

    public List<T> getData() {
        return this.array;
    }
}
