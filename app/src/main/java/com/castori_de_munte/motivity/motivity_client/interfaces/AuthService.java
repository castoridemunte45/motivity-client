package com.castori_de_munte.motivity.motivity_client.interfaces;


import com.castori_de_munte.motivity.motivity_client.models.Auth;
import com.castori_de_munte.motivity.motivity_client.models.SyncActivity;
import com.castori_de_munte.motivity.motivity_client.models.UserObj;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface AuthService {
    @POST("auth/login")
    Call<UserObj> login(@Body Auth auth);

    @POST("sync")
    Call<List<SyncActivity>> sync(@Body Auth.Credentials credentials);

    @GET("auth/logout")
    Call<String> logout();

    @POST("auth/register")
    Call<UserObj> register(@Body UserObj user);

    @GET("auth/check")
    Call<String> checkAvailability();
}
