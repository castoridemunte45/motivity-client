package com.castori_de_munte.motivity.motivity_client.adapters;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.castori_de_munte.motivity.motivity_client.Global;
import com.castori_de_munte.motivity.motivity_client.R;
import com.castori_de_munte.motivity.motivity_client.SharedFunctions;
import com.castori_de_munte.motivity.motivity_client.custom_controls.ISO8601;
import com.castori_de_munte.motivity.motivity_client.models.FriendObj;
import com.castori_de_munte.motivity.motivity_client.models.MissionObj;
import com.castori_de_munte.motivity.motivity_client.services.FriendsService;
import com.castori_de_munte.motivity.motivity_client.services.MissionsService;

import java.util.List;

public class FriendsAdapter extends RecyclerView.Adapter<FriendsAdapter.ViewHolder> {
    private LayoutInflater inflater;
    private List<FriendObj.Friend> friends;
    private Context context;
    private FriendsService friendsService;

    public FriendsAdapter(Context context, List<FriendObj.Friend> friends){
        inflater = LayoutInflater.from(context);
        this.context = context;
        this.friends = friends;
        friendsService =  new FriendsService(context.getApplicationContext(),"delete-friend");
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.fragment_friend_view_holder, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(FriendsAdapter.ViewHolder holder, int position) {
        FriendObj.Friend friend = friends.get(position);
        holder.nickName.setText(friend.getNickname());

        holder.id = friend.getId();
    }

    @Override
    public int getItemCount() {
        return friends.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {
        private TextView nickName;
        private int id;


        public ViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            itemView.setOnLongClickListener(this);
            nickName = (TextView) itemView.findViewById(R.id.nickname);

        }

        @Override
        public void onClick(View view) {
            Intent intent = new Intent("selected-mission");
            intent.putExtra("friendId", id);
            intent.putExtra("nickname", nickName.getText().toString());


            for(int i = 0; i < friends.size(); i++){
                if(friends.get(i).getId() == id){
                    Global.friend = FriendObj.Friend.copyFriend(friends.get(i));
                    Global.friendPosition = i;
                    break;
                }
            }
            LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
        }

        @Override
        public boolean onLongClick(View view) {
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setTitle("Delete friend");
            builder.setMessage("Are you sure you want to delete this friend?");
            builder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    friendsService.delete(id);

                }
            });
            builder.setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                }
            });
            builder.setIcon(android.R.drawable.ic_dialog_alert);
            AlertDialog dialog = builder.create();
            dialog.show();

            return true;
        }
    }
}
