package com.castori_de_munte.motivity.motivity_client.services;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;

import com.castori_de_munte.motivity.motivity_client.Global;
import com.castori_de_munte.motivity.motivity_client.R;
import com.castori_de_munte.motivity.motivity_client.SharedFunctions;
import com.castori_de_munte.motivity.motivity_client.middleware.ServiceCreator;
import com.castori_de_munte.motivity.motivity_client.models.ActivityObj;
import com.castori_de_munte.motivity.motivity_client.models.MissionObj;
import com.castori_de_munte.motivity.motivity_client.models.ResponseList;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivitiesService {
    public HashMap<String, Boolean> actions;
    private Context context;
    private com.castori_de_munte.motivity.motivity_client.interfaces.ActivitiesService activitiesService;
    private String eventName;

    public ActivitiesService(Context context, String eventName){
        this.actions = new HashMap<>();
        this.context = context;
        this.eventName = eventName;
        this.activitiesService =
                ServiceCreator.retrofitService(com.castori_de_munte.motivity.motivity_client.interfaces.ActivitiesService.class, context);
    }

    public void list(Map<String, String> options){
        if ((actions.containsKey("list") && actions.get("list")) || SharedFunctions.checkNetworkStatus(context, activitiesService, eventName)) return;
        else
            actions.put("list", true);
        String url = "user/" + Global.user.getId() + "/activity";
        Call<ResponseList<ActivityObj.Activity>> call = activitiesService.userActivities(url,  options);
        call.enqueue(new Callback<ResponseList<ActivityObj.Activity>>() {
            @Override
            public void onResponse(Call<ResponseList<ActivityObj.Activity>> call, Response<ResponseList<ActivityObj.Activity>> response) {
                int statusCode = response.code();
                actions.remove("list");
                Intent intent = new Intent(eventName);

                if(statusCode == 200) {
                    List<ActivityObj.Activity> activities = response.body().getItems();
                    for(ActivityObj.Activity activity : activities){
                        Global.activities.add(activity);
                    }
                }else {
                    String message = Global.isDev ?
                            response.message() : context.getResources().getString(R.string.error_generic);
                    intent.putExtra("error", message);
                }

                LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
            }

            @Override
            public void onFailure(Call<ResponseList<ActivityObj.Activity>> call, Throwable t) {
                SharedFunctions.failing(context, t, eventName);
            }
        });
    }

    public void get(int activityId){
        if ((actions.containsKey("get") && actions.get("get")) || SharedFunctions.checkNetworkStatus(context, activitiesService, eventName)) return;
        else
            actions.put("get", true);
        String url = "user/" + Global.user.getId() + "/activity/" + activityId;
        Call<ActivityObj> call = activitiesService.get(url);
        call.enqueue(new Callback<ActivityObj>() {
            @Override
            public void onResponse(Call<ActivityObj> call, Response<ActivityObj> response) {
                int statusCode = response.code();
                actions.remove("get");
                Intent intent = new Intent(eventName);

                if(statusCode == 200) {
                    // For further implementation
                }else {
                    String message = Global.isDev ?
                            response.message() : context.getResources().getString(R.string.error_generic);
                    intent.putExtra("error", message);
                }

                LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
            }

            @Override
            public void onFailure(Call<ActivityObj> call, Throwable t) {
                SharedFunctions.failing(context, t, eventName);
            }
        });
    }

    public void insert(ActivityObj activityObj, final boolean appendToList){
        if(appendToList)
            if ((actions.containsKey("insert") && actions.get("insert")) || SharedFunctions.checkNetworkStatus(context, activitiesService, eventName)) return;
            else
                actions.put("insert", true);
        String url = "user/" + Global.user.getId() + "/activity";
        Call<ActivityObj> call = activitiesService.insert(url,activityObj);
        call.enqueue(new Callback<ActivityObj>() {
            @Override
            public void onResponse(Call<ActivityObj> call, Response<ActivityObj> response) {
                int statusCode = response.code();
                actions.remove("insert");
                Intent intent = new Intent(eventName);

                if(statusCode == 200) {
                    if(appendToList) Global.activities.add(response.body().getActivity());
                }else {
                    String message = Global.isDev ?
                            response.message() : context.getResources().getString(R.string.error_generic);
                    intent.putExtra("error", message);
                }

                LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
            }

            @Override
            public void onFailure(Call<ActivityObj> call, Throwable t) {
                SharedFunctions.failing(context, t, eventName);
            }
        });
    }

    public void delete(final int id){
        if ((actions.containsKey("delete") && actions.get("delete")) || SharedFunctions.checkNetworkStatus(context, activitiesService, eventName)) return;
        else
            actions.put("delete", true);
        String url = "user/" + Global.user.getId() + "/activity/" + id;
        Call<String> call = activitiesService.delete(url);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                int statusCode = response.code();
                actions.remove("delete");
                Intent intent = new Intent(eventName);

                if(statusCode == 200) {
                    for(int i = 0; i < Global.activities.size(); i ++){
                        if(Global.activities.get(i).getId() == id){
                            Global.activities.remove(i);
                        }
                    }
                    // For further implentation
                }else {
                    String message = Global.isDev ?
                            response.message() : context.getResources().getString(R.string.error_generic);
                    intent.putExtra("error", message);
                }

                LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                SharedFunctions.failing(context, t, eventName);
            }
        });
    }

    public void update(final ActivityObj activityObj){
        if ((actions.containsKey("update") && actions.get("update")) || SharedFunctions.checkNetworkStatus(context, activitiesService, eventName)) return;
        else
            actions.put("update", true);
        String url = "user/" + Global.user.getId() + "/activity/" + activityObj.getActivity().getId();
        Call<ActivityObj> call = activitiesService.update(url, activityObj);
        call.enqueue(new Callback<ActivityObj>() {
            @Override
            public void onResponse(Call<ActivityObj> call, Response<ActivityObj> response) {
                int statusCode = response.code();
                actions.remove("update");
                Intent intent = new Intent(eventName);

                if(statusCode == 200) {
                    ActivityObj.Activity activity = response.body().getActivity();
                    for(int i = 0; i < Global.activities.size(); i ++){
                        if(Global.activities.get(i).getId() == activityObj.getActivity().getId()){
                            Global.activities.remove(i);
                            Global.activities.add(i, ActivityObj.Activity.copyActivity(activity));
                        }
                    }
                }else {
                    String message = Global.isDev ?
                            response.message() : context.getResources().getString(R.string.error_generic);
                    intent.putExtra("error", message);
                }

                LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
            }

            @Override
            public void onFailure(Call<ActivityObj> call, Throwable t) {
                SharedFunctions.failing(context, t, eventName);
            }
        });
    }
}
