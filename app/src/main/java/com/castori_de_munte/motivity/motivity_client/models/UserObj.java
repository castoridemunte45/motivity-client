package com.castori_de_munte.motivity.motivity_client.models;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class UserObj {
    @JsonCreator
    public UserObj(){
        this.user = new User();
    }

    @JsonProperty("user")
    private User user;

    @JsonProperty("user")
    public User getUser() {
        return user;
    }

    @JsonProperty("user")
    public void setUser(User user) {
        this.user = user;
    }


    public static class User{
        @JsonCreator
        public User() {
            this.firstName = this.lastName = "";
            this.active = true;
            this.isAdmin = false;
        }

        @JsonProperty("id")
        private int id;

        @JsonProperty("first_name")
        private String firstName;

        @JsonProperty("last_name")
        private String lastName;

        @JsonProperty("email")
        private String email;

        @JsonProperty("password")
        private String password;

        @JsonProperty("is_admin")
        private boolean isAdmin;

        @JsonProperty("seen_tutorial")
        private boolean seenTutorial;

        @JsonProperty("active")
        private boolean active;

        @JsonProperty("experience")
        private int experience;

        @JsonProperty("currency")
        private int currency;

        @JsonProperty("user_achievements")
        private List<UserAchievementObj.UserAchievement> userAchievements;

        @JsonProperty("id")
        public int getId() {
            return id;
        }

        @JsonProperty("id")
        public void setId(int id) {
            this.id = id;
        }

        @JsonProperty("first_name")
        public String getFirstName() {
            return firstName;
        }

        @JsonProperty("first_name")
        public void setFirstName(String firstName) {
            this.firstName = firstName;
        }

        @JsonProperty("last_name")
        public String getLastName() {
            return lastName;
        }

        @JsonProperty("last_name")
        public void setLastName(String lastName) {
            this.lastName = lastName;
        }

        @JsonProperty("password")
        public String getPassword() {
            return password;
        }

        @JsonProperty("password")
        public void setPassword(String password) {
            this.password = password;
        }

        @JsonProperty("email")
        public String getEmail() {
            return email;
        }

        @JsonProperty("email")
        public void setEmail(String email) {
            this.email = email;
        }

        @JsonProperty("is_admin")
        public boolean getIsAdmin() {
            return isAdmin;
        }

        @JsonProperty("is_admin")
        public void setIsAdmin(boolean isAdmin) {
            this.isAdmin = isAdmin;
        }

        @JsonProperty("seen_tutorial")
        public boolean getSeenTutorial() {
            return seenTutorial;
        }

        @JsonProperty("seen_tutorial")
        public void setSeenTutorial(boolean seenTutorial) {
            this.seenTutorial = seenTutorial;
        }

        @JsonProperty("active")
        public boolean getActive() {
            return active;
        }

        @JsonProperty("active")
        public void setActive(boolean active) {
            this.active = active;
        }

        @JsonProperty("experience")
        public int getExperience() {
            return experience;
        }

        @JsonProperty("experience")
        public void setExperience(int experience) {
            this.experience = experience;
        }

        @JsonProperty("currency")
        public int getCurrency() {
            return currency;
        }

        @JsonProperty("currency")
        public void setCurrency(int currency) {
            this.currency = currency;
        }

        @JsonProperty("user_achievements")
        public List<UserAchievementObj.UserAchievement> getUserAchievements() {
            return userAchievements;
        }

        @JsonProperty("user_achievements")
        public void setUserAchievements(List<UserAchievementObj.UserAchievement> userAchievements) {
            this.userAchievements = userAchievements;
        }
    }
}