package com.castori_de_munte.motivity.motivity_client.custom_controls;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

/**
 * Helper class for handling a most common subset of ISO 8601 strings
 * (in the following format: "2008-03-01T13:00:00+01:00"). It supports
 * parsing the "Z" timezone, but many other less-used features are
 * missing.
 */
public final class ISO8601 {
    public final static String ISO8601 = "yyyy-MM-dd'T'HH:mm:ss.SSSSSSSZ";

    /** Transform Calendar to ISO 8601 string. */
    public static String fromCalendar(final Calendar calendar) {
        Date date = calendar.getTime();
        String formatted = new SimpleDateFormat(ISO8601)
                .format(date);
        return formatted.substring(0, 22) + ":" + formatted.substring(22);
    }

    /** Get current date and time formatted as ISO 8601 string. */
    public static String now() {
        return fromCalendar(GregorianCalendar.getInstance());
    }

    /** Transform ISO 8601 string to Calendar. */
    public static Calendar toCalendar(final String iso8601string){
        Calendar calendar = GregorianCalendar.getInstance();
        String s = iso8601string.replace("Z", "+00:00");
        try {
            s = s.substring(0, 22) + s.substring(23);  // to get rid of the ":"
        } catch (IndexOutOfBoundsException e) {
            return null;
        }
        Date date = null;
        try {
            date = new SimpleDateFormat(ISO8601).parse(s);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        if(date != null) calendar.setTime(date);
        return calendar;
    }

    public static Calendar getDateAsCalendar(String myDate){
        if(myDate == null) return null;
        SimpleDateFormat sdf = new SimpleDateFormat("EEE MMM dd HH:mm:ss zzz yyyy", Locale.ROOT);
        return toCalendar(myDate);
    }

    public static Date getDateFromString(String myDate){
        Calendar date = getDateAsCalendar(myDate);
        if(date == null) return null;
        return date.getTime();
    }

    public static String formatDate(Calendar date){
        SimpleDateFormat sdf = new SimpleDateFormat("EEE MMM dd HH:mm:ss zzz yyyy", Locale.ROOT);
        Date d = date.getTime();
        String aux[] = sdf.format(d).split(" ");
        return aux[3] + " " + aux[2] + "/" + aux[1] + "/" + aux[5] + "  ";
    }

    public static Calendar dateAddHours(String myDate, int hours){
        Calendar date = toCalendar(myDate);
        if(date == null) return null;
        date.add(Calendar.HOUR_OF_DAY, hours);
        return date;
    }

    public static String friendlyTime(long date1, long date2){
        long diff = date2 - date1;
        if(diff < 0) return null;

        long days = TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
        if(days > 0) {
            return String.valueOf(days) + (days == 1 ? " day" : " days") + " left";
        } else {
            long hours = TimeUnit.HOURS.convert(diff, TimeUnit.MILLISECONDS);
            if(hours > 0) {
                return String.valueOf(hours) + (hours == 1 ? " hour" : " hours") + " left";
            } else {
                long minutes = TimeUnit.MINUTES.convert(diff, TimeUnit.MILLISECONDS);
                return String.valueOf(minutes) + (minutes == 1 ? " minute" : " minutes") + " left";
            }
        }
    }

    public static long dateInMilliseconds(String myDate){
        Calendar date = toCalendar(myDate);
        if(date == null) return 0;
        Date d = date.getTime();
        return d.getTime();
    }

    public static long dateInMilliseconds(Calendar date){
        if(date == null) return 0;
        Date d = date.getTime();
        return d.getTime();
    }

    public static long timeBetweenDatesAsMilliseconds(String myDate1, String myDate2){
        return dateInMilliseconds(myDate2) - dateInMilliseconds(myDate1);
    }

    public static long timeBetweenDatesAsMilliseconds(String myDate1, long myDate2){
        return myDate2 - dateInMilliseconds(myDate1);
    }

    public static long timeBetweenDatesAsMilliseconds(long myDate1, long myDate2){
        return myDate2 - myDate1;
    }

    public static long timeBetweenDatesAsMilliseconds(long myDate1, String myDate2){
        return dateInMilliseconds(myDate2) - myDate1;
    }
}