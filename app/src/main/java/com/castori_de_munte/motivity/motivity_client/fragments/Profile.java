package com.castori_de_munte.motivity.motivity_client.fragments;


import android.app.Fragment;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.AppCompatCheckBox;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;

import com.castori_de_munte.motivity.motivity_client.Global;
import com.castori_de_munte.motivity.motivity_client.R;
import com.castori_de_munte.motivity.motivity_client.SharedFunctions;
import com.castori_de_munte.motivity.motivity_client.models.UserObj;
import com.castori_de_munte.motivity.motivity_client.services.UsersService;

public class Profile extends Fragment {

    private EditText etFirstName;
    private EditText etLastName;
    private EditText etEmail;
    private EditText etPassword;
    private EditText etRepeatPassword;
    private TextInputLayout tilPasswordContainer;
    private TextInputLayout tilRepeatPasswordContainer;
    private AppCompatCheckBox cbChangePassword;
    private Button btUpdateProfile;
    private boolean isChangingPassword = false;

    private BroadcastReceiver receivedUserUpdate = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Bundle bundle = intent.getExtras();
            if(SharedFunctions.intentHasError(context.getApplicationContext(), bundle)){
                return;
            }
            Snackbar.make(getActivity().findViewById(R.id.drawer_layout_home),
                    getActivity().getResources().getString(R.string.information_updated), Snackbar.LENGTH_SHORT).show();
            NavigationView nvNav = (NavigationView) getActivity().findViewById(R.id.navigation_view);
            if (nvNav != null) {
                TextView drawerUser = (TextView) nvNav.getHeaderView(0).findViewById(R.id.drawer_header_user);
                String displayedUser = Global.user.getFirstName() + " " + Global.user.getLastName() + ": " + Global.user.getEmail();
                drawerUser.setText(displayedUser);
            }
        }
    };

    @Override
    public void onDestroy() {
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(receivedUserUpdate);
        super.onDestroy();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(receivedUserUpdate,
                new IntentFilter("received-user-update"));
        return inflater.inflate(R.layout.fragment_profile, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initUI();
    }

    private void initUI() {
        View view = getView();
        if (view != null) {
            etFirstName = (EditText) view.findViewById(R.id.etFirstName);
            etLastName = (EditText) view.findViewById(R.id.etLastName);
            etEmail = (EditText) view.findViewById(R.id.etEmail);
            etEmail.setKeyListener(null);
            etPassword = (EditText) view.findViewById(R.id.etPassword);
            etRepeatPassword = (EditText) view.findViewById(R.id.etRepeatPassword);
            tilPasswordContainer = (TextInputLayout) view.findViewById(R.id.etPasswordContainer);
            tilRepeatPasswordContainer = (TextInputLayout) view.findViewById(R.id.etRepeatPasswordContainer);
            cbChangePassword = (AppCompatCheckBox) view.findViewById(R.id.cbChangePassword);
            btUpdateProfile = (Button) view.findViewById(R.id.btUpdateProfile);

            cbChangePassword.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    if (b) {
                        tilPasswordContainer.setVisibility(View.VISIBLE);
                        tilRepeatPasswordContainer.setVisibility(View.VISIBLE);
                    } else {
                        tilPasswordContainer.setVisibility(View.GONE);
                        tilRepeatPasswordContainer.setVisibility(View.GONE);
                    }
                    isChangingPassword = b;
                }
            });

            btUpdateProfile.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    boolean ok = true;

                    if (isChangingPassword) {
                        if (etPassword.getText().toString().isEmpty() || etRepeatPassword.getText().toString().isEmpty()) {
                            if (etPassword.getText().toString().isEmpty())
                                etPassword.setError(getResources().getString(R.string.field_required));
                            if (etRepeatPassword.getText().toString().isEmpty())
                                etRepeatPassword.setError(getResources().getString(R.string.field_required));
                            Snackbar.make(view.findViewById(R.id.drawer_layout_home), R.string.fields_empty, Snackbar.LENGTH_SHORT).show();
                            ok = false;
                        }

                        if (!etPassword.getText().toString().equals(etRepeatPassword.getText().toString())) {
                            etRepeatPassword.setError(getResources().getString(R.string.passwords_mismatch));
                            Snackbar.make(view.findViewById(R.id.drawer_layout_home), R.string.passwords_mismatch, Snackbar.LENGTH_SHORT).show();
                            ok = false;
                        }
                    }

                    if (!ok) return;

                    UserObj userObj = new UserObj();
                    userObj.getUser().setFirstName(etFirstName.getText().toString());
                    userObj.getUser().setLastName(etLastName.getText().toString());
                    userObj.getUser().setEmail(etEmail.getText().toString());
                    if (isChangingPassword)
                        userObj.getUser().setPassword(etPassword.getText().toString());
                    userObj.getUser().setActive(Global.user.getActive());
                    userObj.getUser().setIsAdmin(Global.user.getIsAdmin());
                    userObj.getUser().setSeenTutorial(Global.user.getSeenTutorial());
                    UsersService usersService = new UsersService(view.getContext(), "received-user-update");
                    usersService.update(userObj);
                }
            });

            UserObj.User user = Global.user;
            etFirstName.setText(user.getFirstName());
            etLastName.setText(user.getLastName());
            etEmail.setText(user.getEmail());
        }
    }

}
