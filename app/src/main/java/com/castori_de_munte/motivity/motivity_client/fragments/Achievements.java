package com.castori_de_munte.motivity.motivity_client.fragments;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.castori_de_munte.motivity.motivity_client.Global;
import com.castori_de_munte.motivity.motivity_client.R;
import com.castori_de_munte.motivity.motivity_client.SharedFunctions;
import com.castori_de_munte.motivity.motivity_client.adapters.UserAchievementsAdapter;
import com.castori_de_munte.motivity.motivity_client.custom_controls.EndlessScrollListener;
import com.castori_de_munte.motivity.motivity_client.services.UserAchievementsService;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class Achievements extends Fragment {
    private LinearLayoutManager llmRecycleLayout;
    private RecyclerView rvRecycler;
    private FrameLayout flFallback;
    private SwipeRefreshLayout srlSwipeRefresh;

    private UserAchievementsService userAchievementsService;
    private UserAchievementsAdapter userAchievementsAdapter;


    private BroadcastReceiver receivedAchievements = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Bundle bundle = intent.getExtras();
            if(SharedFunctions.intentHasError(context.getApplicationContext(), bundle)){
                return;
            }
            showAchievements();
        }
    };

    private BroadcastReceiver selectedAchievement = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Bundle bundle = intent.getExtras();
            Intent viewMissionIntent = new Intent("change-fragment");
            viewMissionIntent.putExtra("fragmentType", "selected-achievement");
            viewMissionIntent.putExtras(bundle);
            LocalBroadcastManager.getInstance(context).sendBroadcast(viewMissionIntent);
        }
    };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(receivedAchievements,
                new IntentFilter("received-achievements"));
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(selectedAchievement,
                new IntentFilter("selected-achievement"));
        return inflater.inflate(R.layout.fragment_user_achievements, container, false);
    }

    @Override
    public void onDestroy() {
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(receivedAchievements);
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(selectedAchievement);
        super.onDestroy();
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initUI();

        userAchievementsService = new UserAchievementsService(getActivity().getApplicationContext(), "received-achievements");
        if(Global.userAchievements.size() == 0){
            userAchievementsService.list(Global.defaultOptions);
        }else{
            showAchievements();
            rvRecycler.scrollToPosition(Global.userAchievementPosition);
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.toolbar, menu);
        initClearFilters(menu);
        initSearchView(menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    private void initClearFilters(final Menu menu){
        final MenuItem clearFilters = menu.findItem(R.id.action_clear_filters);
        clearFilters.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                inputSearchQuery(menu, "");
                Global.userAchievementsOptions.remove("keyword");
                srlSwipeRefresh.setRefreshing(true);
                rvRecycler.setVisibility(View.GONE);
                Map<String, String> options = new HashMap<>(Global.defaultOptions);
                searchAchievements(options);
                return true;
            }
        });
    }

    private void inputSearchQuery(Menu menu, String query){
        final MenuItem searchMenu = menu.findItem(R.id.action_search);
        final SearchView searchView = (SearchView) searchMenu.getActionView();

        if(query.isEmpty()){
            searchMenu.collapseActionView();
        }else {
            searchMenu.expandActionView();
        }
        searchView.setIconified(false);
        searchView.setQuery(query, false);
        searchView.clearFocus();
    }

    private void initSearchView(Menu menu){
        final MenuItem searchMenu = menu.findItem(R.id.action_search);
        final SearchView searchView = (SearchView) searchMenu.getActionView();

        searchView.setIconifiedByDefault(false);

        ImageView closeButton = (ImageView) searchView.findViewById(R.id.search_close_btn);
        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText et = (EditText) searchView.findViewById(R.id.search_src_text);
                et.setText("");
                searchView.setQuery("", false);
                searchView.onActionViewCollapsed();
                searchMenu.collapseActionView();
                searchView.clearFocus();
                Map<String, String> options = new HashMap<>(Global.defaultOptions);
                options.put("keyword", "");
                searchAchievements(options);
            }
        });
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                searchView.clearFocus();
                Map<String, String> options = new HashMap<>(Global.defaultOptions);
                if(!query.isEmpty())
                    options.put("keyword", query);
                searchAchievements(options);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                return false;
            }
        });
    }

    private void searchAchievements(Map<String, String> attrs){
        Global.userAchievements.clear();
        Global.userAchievementPosition = -1;
        setScrollListeners();

        if(attrs != null) {
            Iterator it = attrs.entrySet().iterator();
            while (it.hasNext()) {
                Map.Entry pair = (Map.Entry) it.next();
                String key = (String) pair.getKey(), value = (String) pair.getValue();
                Global.userAchievementsOptions.put(key, value);
                it.remove(); // avoids a ConcurrentModificationException
            }
        }else{
            Global.userAchievementsOptions = new HashMap<>(Global.defaultOptions);
        }
        userAchievementsService.list(Global.userAchievementsOptions);
    }

    private void initUI(){
        View view = getView();
        if(view != null) {
            llmRecycleLayout = new LinearLayoutManager(getActivity());
            flFallback = (FrameLayout) view.findViewById(R.id.fallback_frame);
            rvRecycler = (RecyclerView) view.findViewById(R.id.recycler_view_active_missions);
            srlSwipeRefresh = (SwipeRefreshLayout) view.findViewById(R.id.swipe_refresh);

            setScrollListeners();

            srlSwipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                @Override
                public void onRefresh() {
                    Global.userAchievements.clear();
                    Global.userAchievementsOptions.put("page", Global.defaultOptions.get("page"));
                    rvRecycler.setVisibility(View.GONE);
                    userAchievementsService.list(Global.userAchievementsOptions);
                    setScrollListeners();
                }
            });

            FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
            Loading loading = new Loading();
            fragmentTransaction.replace(R.id.fallback_frame, loading);
            fragmentTransaction.commit();
        }
    }

    private void setScrollListeners(){
        rvRecycler.clearOnScrollListeners();
        rvRecycler.addOnScrollListener(new EndlessScrollListener(llmRecycleLayout) {
            @Override
            public void onLoadMore(int page, int totalItemsCount) {
                if(Integer.parseInt(Global.userAchievementsOptions.get("page")) > page) return;
                Global.userAchievementsOptions.put("page", String.valueOf(page));
                userAchievementsService.list(Global.userAchievementsOptions);
            }
        });
        rvRecycler.addOnScrollListener(new RecyclerView.OnScrollListener(){
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy){
//                if (dy > 0 || dy<0 && fab.isShown())
//                    fab.hide();
            }

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {

                if (newState == AbsListView.OnScrollListener.SCROLL_STATE_IDLE){
//                    fab.show();
                }
                super.onScrollStateChanged(recyclerView, newState);
            }
        });
    }

    private void showAchievements(){
        if(srlSwipeRefresh.isRefreshing()){
            srlSwipeRefresh.setRefreshing(false);
        }

        flFallback.setVisibility(View.GONE);
        rvRecycler.setVisibility(View.VISIBLE);
        if(userAchievementsAdapter == null) {
            userAchievementsAdapter = new UserAchievementsAdapter(getActivity(), Global.userAchievements);
            rvRecycler.setAdapter(userAchievementsAdapter);
            rvRecycler.setLayoutManager(llmRecycleLayout);
        }else{
            userAchievementsAdapter.notifyDataSetChanged();
        }
    }
}
