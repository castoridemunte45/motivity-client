package com.castori_de_munte.motivity.motivity_client;

import android.app.FragmentTransaction;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.wifi.WifiManager;
import android.support.design.widget.Snackbar;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;

import com.castori_de_munte.motivity.motivity_client.custom_controls.AutoResizeTextView;
import com.castori_de_munte.motivity.motivity_client.custom_controls.NetworkStatus;
import com.castori_de_munte.motivity.motivity_client.fragments.Loading;
import com.castori_de_munte.motivity.motivity_client.services.AuthService;


public class ServerActivity extends AppCompatActivity {

    private FrameLayout flFallback;
    private AutoResizeTextView tvServerDown;
    private Button btRetry;

    private BroadcastReceiver receivedConnection = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Bundle bundle = intent.getExtras();
            if(SharedFunctions.intentHasError(context.getApplicationContext(), bundle)){
                handleError();
                return;
            }
            handleConnection();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_server);

        LocalBroadcastManager.getInstance(this).registerReceiver(receivedConnection,
                new IntentFilter("received-connection"));

        tvServerDown = (AutoResizeTextView) findViewById(R.id.tvServerDown);
        flFallback = (FrameLayout) findViewById(R.id.fallback_frame);
        btRetry = (Button) findViewById(R.id.btRetry);

        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
        Loading loading = new Loading();
        fragmentTransaction.replace(R.id.fallback_frame, loading);
        fragmentTransaction.commit();

        checkInternetConnection();
    }



    @Override
    public void onDestroy() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(receivedConnection);
        super.onDestroy();
    }

    private void checkInternetConnection(){
        if(!NetworkStatus.getInstance(getApplicationContext()).isNetworkAvailable()){
            AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.AlertDialog);
            builder.setTitle(getResources().getString(R.string.you_are_offline));
            builder.setMessage(getResources().getString(R.string.enable_wifi_message));
            builder.setPositiveButton(getResources().getString(R.string.ok),
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            WifiManager wifi = (WifiManager) getSystemService(Context.WIFI_SERVICE);
                            wifi.setWifiEnabled(true);
                            checkAvailability();
                        }
                    });
            builder.setNegativeButton(getResources().getString(R.string.cancel),
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            checkInternetConnection();
                        }
                    });
            builder.show();
        }else{
            checkAvailability();
        }
    }

    private void checkAvailability(){
        AuthService authService = new AuthService(getApplicationContext(), "received-connection");
        authService.checkAvailability();
    }

    private void handleConnection(){
        Intent serverActivity = new Intent(ServerActivity.this, LoginActivity.class);
        startActivity(serverActivity);
    }

    private void handleError(){
        flFallback.setVisibility(View.GONE);
        tvServerDown.setVisibility(View.VISIBLE);
        btRetry.setVisibility(View.VISIBLE);
    }

    public void retry(View view){
        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
        Loading loading = new Loading();
        fragmentTransaction.replace(R.id.fallback_frame, loading);
        fragmentTransaction.commit();

        flFallback.setVisibility(View.VISIBLE);
        tvServerDown.setVisibility(View.GONE);
        btRetry.setVisibility(View.GONE);
        checkAvailability();
    }
}
