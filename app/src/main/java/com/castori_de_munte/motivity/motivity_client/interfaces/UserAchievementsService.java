package com.castori_de_munte.motivity.motivity_client.interfaces;

import com.castori_de_munte.motivity.motivity_client.models.ActivityObj;
import com.castori_de_munte.motivity.motivity_client.models.ResponseList;
import com.castori_de_munte.motivity.motivity_client.models.UserAchievementObj;

import java.util.Map;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.QueryMap;
import retrofit2.http.Url;

public interface UserAchievementsService {
    @GET
    Call<ResponseList<UserAchievementObj.UserAchievement>> userAchievements(
            @Url String url, @QueryMap Map<String, String> options
    );

    @GET
    Call<UserAchievementObj> get(@Url String url);

    @PUT
    Call<UserAchievementObj> update(@Url String url, @Body UserAchievementObj userAchievementObj);

    @POST
    Call<UserAchievementObj> insert(@Url String url, @Body UserAchievementObj userAchievementObj);

    @DELETE
    Call<String> delete(@Url String url);
}
