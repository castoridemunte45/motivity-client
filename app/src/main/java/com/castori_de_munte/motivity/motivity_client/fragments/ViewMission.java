package com.castori_de_munte.motivity.motivity_client.fragments;


import android.app.AlertDialog;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.app.Fragment;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.TextView;

import com.castori_de_munte.motivity.motivity_client.Global;
import com.castori_de_munte.motivity.motivity_client.R;
import com.castori_de_munte.motivity.motivity_client.SharedFunctions;
import com.castori_de_munte.motivity.motivity_client.adapters.ActivitiesAdapter;
import com.castori_de_munte.motivity.motivity_client.custom_controls.ISO8601;
import com.castori_de_munte.motivity.motivity_client.custom_controls.SnackbarWrapper;
import com.castori_de_munte.motivity.motivity_client.models.ActivityObj;
import com.castori_de_munte.motivity.motivity_client.models.MissionObj;
import com.castori_de_munte.motivity.motivity_client.services.ActivitiesService;
import com.castori_de_munte.motivity.motivity_client.services.MissionsService;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class ViewMission extends Fragment {

    private LinearLayoutManager llmRecycleLayout;
    private RecyclerView rvRecycler;
    private FrameLayout flFallback;
    private ProgressBar pbMissionProgress;
    private ProgressBar pbProgressTime;
    private TextView tvMissionProgress;
    private TextView tvTitle;
    private TextView tvDescription;

    private TextView tvCreatedAt;
    private TextView tvTimeLeft;
    private ImageView ivUpdate;
    private int id;

    private AlertDialog dialog;

    private MissionsService missionsService;
    private ActivitiesAdapter activitiesAdapter;

    private BroadcastReceiver receivedMission = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Bundle bundle = intent.getExtras();
            if(SharedFunctions.intentHasError(context.getApplicationContext(), bundle)){
                return;
            }
            loadMission();
        }
    };

    private BroadcastReceiver updatedMission = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Bundle bundle = intent.getExtras();
            if(SharedFunctions.intentHasError(context.getApplicationContext(), bundle)){
                return;
            }
            reloadMission();
        }
    };

    private BroadcastReceiver updatedCompletedMission = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Bundle bundle = intent.getExtras();
            if(SharedFunctions.intentHasError(context.getApplicationContext(), bundle)){
                return;
            }
            loadMission();
        }
    };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(receivedMission,
                new IntentFilter("received-mission"));
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(updatedMission,
                new IntentFilter("updated-mission"));
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(updatedCompletedMission,
                new IntentFilter("updated-completed-mission"));
        return inflater.inflate(R.layout.fragment_view_mission, container, false);
    }

    @Override
    public void onDestroy() {
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(receivedMission);
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(updatedMission);
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(updatedCompletedMission);
        super.onDestroy();
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initUI();

        missionsService = new MissionsService(getActivity().getApplicationContext(), "received-mission");
        missionsService.get(id);
    }

    private void initUI() {
        View view = getView();
        if (view != null) {
            llmRecycleLayout = new LinearLayoutManager(getActivity());
            rvRecycler = (RecyclerView) view.findViewById(R.id.recycler_view_activities);
            flFallback = (FrameLayout) view.findViewById(R.id.fallback_frame);
            tvTitle = (TextView) view.findViewById(R.id.tvTitle);
            tvDescription = (TextView) view.findViewById(R.id.tvDescription);
            tvCreatedAt = (TextView) view.findViewById(R.id.tvCreatedAt);
            tvTimeLeft = (TextView) view.findViewById(R.id.tvTimeLeft);
            ivUpdate = (ImageView) view.findViewById(R.id.ivUpdate);
            tvMissionProgress = (TextView) view.findViewById(R.id.tvProgress);

            pbMissionProgress = (ProgressBar) view.findViewById(R.id.pbProgress);
            pbMissionProgress.getProgressDrawable().setColorFilter(
                    ContextCompat.getColor(view.getContext(), R.color.red), android.graphics.PorterDuff.Mode.SRC_IN);
            pbProgressTime = (ProgressBar) view.findViewById(R.id.pbProgressTime);
            pbProgressTime.getProgressDrawable().setColorFilter(
                    ContextCompat.getColor(view.getContext(),R.color.blue), android.graphics.PorterDuff.Mode.SRC_IN);

            String expiredTime =  ISO8601.fromCalendar(ISO8601.dateAddHours(Global.mission.getCreatedAt(),Global.mission.getDuration()));
            float timePassed = ISO8601.timeBetweenDatesAsMilliseconds(ISO8601.now(),expiredTime);
            if(timePassed<=0 && !Global.mission.getIsCompleted())
                Global.mission.setExpired(true);

            if(Global.mission.getIsExpired()){
                ivUpdate.setBackgroundResource(R.drawable.ic_close_circle);
                ivUpdate.setOnClickListener(null);
                pbProgressTime.setVisibility(View.GONE);
                tvCreatedAt.setVisibility(View.GONE);
                tvTimeLeft.setVisibility(View.GONE);
            }else if(!Global.mission.getIsCompleted()) {
                ivUpdate.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(final View view) {
                        final TextView tvEndValueSeekBar;
                        AlertDialog.Builder builder = new AlertDialog.Builder(view.getContext());
                        LayoutInflater inflater = (LayoutInflater) view.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                        View layout = inflater.inflate(R.layout.dialog_mission_update,
                                (ViewGroup) view.findViewById(R.id.root_element));

                        tvEndValueSeekBar = (TextView) layout.findViewById(R.id.tvEndValueSeekBar);
                        int finalValue = Global.mission.getMilestone() - Global.mission.getProgress();
                        tvEndValueSeekBar.setText(String.valueOf(finalValue));

                        builder.setTitle(view.getResources().getString(R.string.update_mission));

                        final SeekBar seekBar = (SeekBar) layout.findViewById(R.id.seekbar);
                        seekBar.setMax(Global.mission.getMilestone() - Global.mission.getProgress());

                        final TextView tvAddProgress = (TextView) layout.findViewById(R.id.tvAddProgress);

                        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                            @Override
                            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                                String text = "Completed " + i + (i == 1 ? " hour" : " hours");
                                tvAddProgress.setText(text);
                            }

                            @Override
                            public void onStartTrackingTouch(SeekBar seekBar) {

                            }

                            @Override
                            public void onStopTrackingTouch(SeekBar seekBar) {

                            }
                        });

                        builder.setPositiveButton(R.string.ok, null);
                        builder.setNegativeButton(R.string.cancel, null);

                        builder.setIcon(android.R.drawable.arrow_up_float);
                        builder.setView(layout);
                        dialog = builder.create();
                        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
                            @Override
                            public void onShow(DialogInterface dialog) {
                                Button button = ((AlertDialog) dialog).getButton(AlertDialog.BUTTON_POSITIVE);
                                button.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        int progress = seekBar.getProgress();
                                        Global.mission.setProgress(Global.mission.getProgress() + progress);
                                        MissionObj mission = new MissionObj();
                                        mission.setMission(MissionObj.Mission.copyMission(Global.mission));
                                        missionsService = new MissionsService(view.getContext(), "updated-mission");
                                        missionsService.update(mission);
                                    }
                                });
                            }
                        });
                        dialog.show();
                    }
                });
            } else {
                ivUpdate.setBackgroundResource(R.drawable.ic_check_circle);
                ivUpdate.setOnClickListener(null);
                pbProgressTime.setVisibility(View.GONE);
                tvCreatedAt.setVisibility(View.GONE);
                tvTimeLeft.setVisibility(View.GONE);
            }

            Bundle extras = getArguments();
            if(extras != null) {
                tvTitle.setText(extras.getString("title"));
                tvDescription.setText(extras.getString("description"));
                tvMissionProgress.setText(extras.getString("progressText"));
                SharedFunctions.setMax(pbMissionProgress, extras.getInt("milestone"), 100);
                SharedFunctions.setProgress(pbMissionProgress, extras.getInt("progress"), 100);
                id = extras.getInt("missionId");
                tvCreatedAt.setText(extras.getString("createdAtText"));
                tvTimeLeft.setText(extras.getString("expiresAtText"));
                SharedFunctions.setMax(pbProgressTime, (int)extras.getLong("timeMax"), 1f);
                SharedFunctions.setProgress(pbProgressTime, (int)extras.getLong("timeProgress"), 1f);
            }
        }
    }

    private void reloadMission(){
        dialog.dismiss();
        if(!Global.mission.getIsCompleted()) {
            SnackbarWrapper snackbarWrapper = SnackbarWrapper.make(getView().getContext(),
                    getView().getContext().getString(R.string.mission_updated),
                    Snackbar.LENGTH_SHORT);
            snackbarWrapper.show();
        }
        setPercentage();

        if(Global.mission.getIsCompleted()){
            SweetAlertDialog sd =  new SweetAlertDialog(getView().getContext());
            sd.setTitleText(getString(R.string.mission_good_job));
            sd.setContentText(getView().getContext().getString(R.string.mission_completed));
            sd.setCancelable(true);
            sd.setCanceledOnTouchOutside(true);
            sd.show();
            ivUpdate.setBackgroundResource(R.drawable.ic_check_circle);
            ivUpdate.setOnClickListener(null);
            pbProgressTime.setVisibility(View.GONE);
            tvCreatedAt.setVisibility(View.GONE);
            tvTimeLeft.setVisibility(View.GONE);

            MissionsService missionsService = new MissionsService(getView().getContext(),
                    "updated-completed-mission");
            for(int i = 0; i < Global.mission.getActivities().size(); i++){
                Global.mission.getActivities().get(i).setExperience(
                        Global.mission.getActivities().get(i).getExperience() +
                                SharedFunctions.calculateRewardedExp(Global.mission.getActivities().get(i), Global.mission)
                );
                if(Global.mission.getActivities().get(i).getExperience() >= Global.mission.getActivities().get(i).getNextLevelExp()){
                    int difference = Global.mission.getActivities().get(i).getExperience() - Global.mission.getActivities().get(i).getNextLevelExp();
                    do {
                        Global.mission.getActivities().get(i).setExperience(difference);
                        Global.mission.getActivities().get(i).setLevel(Global.mission.getActivities().get(i).getLevel() + 1);
                        Global.mission.getActivities().get(i).setNextLevelExp(
                                SharedFunctions.calculateNextLevelExp(Global.mission.getActivities().get(i))
                        );
                        difference = Global.mission.getActivities().get(i).getExperience() - Global.mission.getActivities().get(i).getNextLevelExp();
                    }while(difference > 0);
                }
            }

            MissionObj mission = new MissionObj();
            mission.setMission(MissionObj.Mission.copyMission(Global.mission));
            missionsService.update(mission);
        }
    }

    private void loadMission() {
        setPercentage();
        activitiesAdapter = new ActivitiesAdapter(getActivity(), Global.mission.getActivities());
        rvRecycler.setAdapter(activitiesAdapter);
        rvRecycler.setLayoutManager(llmRecycleLayout);
    }

    private void setPercentage(){
        float percentage = Global.mission.getProgress() * 100 / Global.mission.getMilestone();
        String textProgress = "In progress: " + percentage + " %";

        String expiredTime =  ISO8601.fromCalendar(ISO8601.dateAddHours(Global.mission.getCreatedAt(),Global.mission.getDuration()));
        float timePassed = ISO8601.timeBetweenDatesAsMilliseconds(ISO8601.now(),expiredTime);
        if(timePassed<=0 && !Global.mission.getIsCompleted())
            textProgress= getResources().getString(R.string.expired);
        else if(Global.mission.getIsCompleted())
            textProgress= getResources().getString(R.string.completed);

        tvMissionProgress.setText(textProgress);
        SharedFunctions.setProgress(pbMissionProgress, Global.mission.getProgress(), 100);
//        pbMissionProgress.setProgress(Global.mission.getProgress());
    }
}